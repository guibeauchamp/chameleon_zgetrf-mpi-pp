/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2017 Inria. All rights reserved.
 * @copyright (c) 2012-2017 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 * @file core_zlaswp.c
 *
 *  PLASMA core_blas kernel
 *  PLASMA is a software package provided by Inria Bordeaux - Sud-Ouest, LaBRI,
 *  University of Bordeaux, Bordeaux INP
 *
 * @version 2.6.0
 * @comment This file has been automatically generated
 *          from Plasma 2.6.0 for MORSE 1.0.0
 * @author Mathieu Faverge
 * @author Terry Cojean
 * @date 2010-11-15
 * @precisions normal z -> c d s
 *
 **/
#include "coreblas/include/coreblas.h"
#include "control/descriptor.h"

#define A(m, n) ((MORSE_Complex64_t *)morse_getaddr_ccrb(descA, m, n))

/***************************************************************************//**
 *
 * @ingroup CORE_MORSE_Complex64_t
 *
 *  CORE_zswpp apply the zlaswp function between a pair of tiles
 *
 *******************************************************************************
 *
 *  @param[in,out] descA
 *          The descriptor of the matrix A to permute, containing both tiles.
 *
 *  @param[in] Bm
 *          The indice of the destination tile
 *
 *  @param[in] i1
 *          The first element of IPIV for which a row interchange will
 *          be done.
 *
 *  @param[in] i2
 *          The last element of IPIV for which a row interchange will
 *          be done.
 *
 *  @param[in] ipiv
 *          The pivot indices; Only the element in position i1 to i2
 *          are accessed. The pivot are offset by A.i.
 *
 *  @param[in] inc
 *          The increment between successive values of IPIV.  If IPIV
 *          is negative, the pivots are applied in reverse order.
 *
 *******************************************************************************
 *
 * @return
 *         \retval MORSE_SUCCESS successful exit
 *         \retval <0 if INFO = -k, the k-th argument had an illegal value
 *
 *******************************************************************************
 */
int CORE_zswpp(MORSE_desc_t* descA, int Bm, int i1, int i2, const int *ipiv, int inc)
{
    /* Change i1 to C notation */
    i1--;

    /* Check parameters */
    if ( descA->nt > 1 ) {
        coreblas_error(1, "Illegal value of descA->nt");
        return -1;
    }
    if ( i1 < 0 ) {
        coreblas_error(2, "Illegal value of i1");
        return -2;
    }
    if ( (i2 <= i1) || (i2 > descA->m) ) {
        coreblas_error(3, "Illegal value of i2");
        return -3;
    }
    if ( ! ( (i2 - i1 - i1%descA->mb -1) < descA->mb ) ) {
        coreblas_error(2, "Illegal value of i1,i2. They have to be part of the same block.");
        return -3;
    }

    if (inc > 0) {
        int it, j, lda1;
        MORSE_Complex64_t *A1;
        it = i1 / descA->mb;
        A1 = A(it, 0);
        lda1 = BLKLDD(descA, 0);

        for (j = i1; j < i2; ++j, ipiv+=inc) {
            int ip = (*ipiv) - descA->i - 1;
            it = ip / descA->mb;

            if ( it == Bm && ip != j)
            {
                int i  = ip % descA->mb;
                int lda2 = BLKLDD(descA, it );
                cblas_zswap(descA->n, A1 + j, lda1,
                            A(it,0) + i, lda2 );
            }
        }
    }

    return MORSE_SUCCESS;
}

/***************************************************************************//**
 *
 * @ingroup CORE_MORSE_Complex64_t
 *
 *  CORE_zswptr_ontile apply the zlaswp function on a matrix stored in
 *  tile layout, followed by a ztrsm on the first tile of the panel.
 *
 *******************************************************************************
 *
 *  @param[in,out] descA
 *          The descriptor of the matrix A to permute.
 *
 *  @param[in] i1
 *          The first element of IPIV for which a row interchange will
 *          be done.
 *
 *  @param[in] i2
 *          The last element of IPIV for which a row interchange will
 *          be done.
 *
 *  @param[in] ipiv
 *          The pivot indices; Only the element in position i1 to i2
 *          are accessed. The pivot are offset by A.i.
 *
 *  @param[in] inc
 *          The increment between successive values of IPIV.  If IPIV
 *          is negative, the pivots are applied in reverse order.
 *
 *  @param[in] Akk
 *          The triangular matrix Akk. The leading descA->nb-by-descA->nb
 *          lower triangular part of the array Akk contains the lower triangular matrix, and the
 *          strictly upper triangular part of A is not referenced. The
 *          diagonal elements of A are also not referenced and are assumed to be 1.
 *
 * @param[in] ldak
 *          The leading dimension of the array Akk. ldak >= max(1,descA->nb).
 *
 *******************************************************************************
 *
 * @return
 *         \retval MORSE_SUCCESS successful exit
 *         \retval <0 if INFO = -k, the k-th argument had an illegal value
 *
 *******************************************************************************
 */

int CORE_zswptr_ontile(MORSE_desc_t *descA, int i1, int i2, const int *ipiv, int inc,
                       const MORSE_Complex64_t *Akk, int ldak)
{
    MORSE_Complex64_t zone  = 1.0;
    int lda;
    int m = descA->mt == 1 ? descA->m : descA->mb;

    if ( descA->nt > 1 ) {
        coreblas_error(1, "Illegal value of descA->nt");
        return -1;
    }
    if ( i1 < 1 ) {
        coreblas_error(2, "Illegal value of i1");
        return -2;
    }
    if ( (i2 < i1) || (i2 > m) ) {
        coreblas_error(3, "Illegal value of i2");
        return -3;
    }

    CORE_zlaswp_ontile(descA, i1, i2, ipiv, inc);

    lda = BLKLDD(descA, 0);
    cblas_ztrsm( CblasColMajor, CblasLeft, CblasLower,
                 CblasNoTrans, CblasUnit,
                 m, descA->n, CBLAS_SADDR(zone),
                 Akk,     ldak,
                 A(0, 0), lda );

    return MORSE_SUCCESS;
}

void CORE_zswp_insert_rows(int nb_pivots, int global_MB,
                           int K, int SRC_M, int DST_M,
                           MORSE_Complex64_t *A,
                           int *perm,
                           MORSE_Complex64_t *rows,
                           int NB, int MB, int LDA)
{
    int i, offset=0;

    for(i = 0; i < MB && offset < nb_pivots * NB ; i++) {
        int owner_tile = perm[i] / global_MB;
        int row_index  = perm[i] % global_MB;

        if (perm[i] != i + global_MB*K) {
            if ((owner_tile == SRC_M && DST_M == K) || (SRC_M == K && DST_M == owner_tile)) {
                int insertion_index;
                //redudant but both use case depend on this
                if (DST_M == K || (DST_M == SRC_M && DST_M == K))
                    insertion_index = i;
                else
                    insertion_index = row_index;
                cblas_zcopy(NB, rows + offset, 1,
                            A + insertion_index, LDA);
                offset += NB;
            }
        }
    }
}

void CORE_zswp_extract_rows(int nb_pivots, int global_MB,
                            int K, int SRC_M, int DST_M,
                            MORSE_Complex64_t *A,
                            int *perm,
                            MORSE_Complex64_t *rows,
                            int NB, int MB, int LDA)
{
    int i, offset=0;

    for (i = 0 ; i < MB && offset < nb_pivots * NB ; i++) {
        int owner_tile = perm[i] / global_MB;
        int row_index  = perm[i] % global_MB;

        /* Do we need to extract something ? */
        if (perm[i] != i + global_MB*K) {
            /* Concerns us somehow */
            if ((owner_tile == SRC_M && DST_M == K) || (SRC_M == K && DST_M == owner_tile)) {
                int extraction_index;
                if (SRC_M == DST_M && SRC_M == K)
                    extraction_index = row_index;
                else if (SRC_M == K) //tile superior
                    extraction_index = i;
                else //tile inferior
                    extraction_index = row_index;
                cblas_zcopy(NB, A + extraction_index, LDA,
                                rows + offset, 1);
                offset += NB;
            }
        }
    }
}
