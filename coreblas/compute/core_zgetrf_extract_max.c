/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2017 Inria. All rights reserved.
 * @copyright (c) 2012-2017 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 * @file core_zgetrf_extract_max.c
 *
 *  MORSE core_blas kernel
 *  MORSE is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver,
 *  and INRIA Bordeaux Sud-Ouest
 *
 * @version 1.0.0
 * @author Omar Zenati
 * @author Terry Cojean
 * @date 2011-11-11
 * @precisions normal z -> s d c
 *
 **/
#include "coreblas/include/coreblas.h"

#define get_pivot(_a_) (*((int *) (_a_)))
#define get_diagonal(_a_) (_a_ + 1)
#define get_maxrow(_a_) (_a_ + NB + 1)

static MORSE_Complex64_t mzone = -1.;

void CORE_zgetrf_extract_max(MORSE_Complex64_t *A, MORSE_Complex64_t *mw,
                             int K, int M, int N, int H, int NB,
                             int AM, int LDA)
{
    int p;
    p = (AM == K)? H : 0;
    M -= p;

    if(H != 0) {
        MORSE_Complex64_t alpha;
        MORSE_Complex64_t *rmax = get_maxrow(mw);
        int pivot = get_pivot(mw);
        int owner_last_index = pivot / NB;
        int last_index       = pivot % NB;

        /* Applying the previous swap */
        if( AM == K /* && (pivot != H-1) */ ) {
            cblas_zcopy(N, get_maxrow(mw)       , 1,
                        A + H-1 , LDA);
        }

        /* Copy the diagonal in place */
        if( owner_last_index == AM /* && (pivot != H-1) */ ) {
            cblas_zcopy(N, get_diagonal(mw)             ,  1,
                        A + last_index, LDA);
        }

        /* Applying the update */
        alpha = 1. / rmax[H-1];

        /* printf("Le pivot a h=%d est localement a %d et globalement a %d et sa valeur est %e\n",H-1,last_index,get_pivot(mw), rmax[H-1]); */

        cblas_zscal(M, CBLAS_SADDR( alpha ), A + (LDA*(H-1) + p), 1 );

        cblas_zgeru(CblasColMajor, M, N-H,
                    CBLAS_SADDR(mzone),
                    A + (LDA*(H-1) + p) , 1   ,
                    get_maxrow(mw) + H               , 1   ,
                    A + (LDA*H + p)     , LDA);
    }


    /* if(H != (((M+p)<N)?M+p:N)) { */
    int index_max = cblas_izamax( M, A + LDA*H + p, 1 ) + p;

    get_pivot(mw) = index_max + AM * NB;

    /* Copy the diagonal row */
    if(K == AM)
        cblas_zcopy(N, A                + H, LDA,
                    get_diagonal(mw)    , 1);
    /* Copy the maximum row */
    cblas_zcopy(N, A              + index_max,  LDA,
                get_maxrow(mw)            , 1);
    /* MORSE_Complex64_t *rmax = get_maxrow(mw); */
    /* if(H == 0) */
    /* printf("m=%d, max in %d = %e, M=%d, p=%d\n",AM,get_pivot(mw),rmax[H],M,p); */
    /* } */

}
