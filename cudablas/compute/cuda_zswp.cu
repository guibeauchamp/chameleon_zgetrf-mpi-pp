/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2017 Inria. All rights reserved.
 * @copyright (c) 2012-2017 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 * @file cuda_zswp.cu
 *
 *  MORSE cudablas kernel
 *  MORSE is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver,
 *  and INRIA Bordeaux Sud-Ouest
 *
 * @author Terry Cojean
 * @date 2017-02-10
 * @precisions normal z -> c d s
 *
 **/
#include "cudablas/include/cudablas.h"
#include "cudablas/include/cudablas_z.h"

/* TODO: look at the LDA, why is it not needed */
__global__ void CUDA_zswp_insert_rows_kernel(int global_MB, int *perm,
                                             cuDoubleComplex *A,
                                             cuDoubleComplex *rows,
                                             int K, int SRC_M, int DST_M,
                                             int NB, int MB, int LDA)
{
	int n = blockIdx.x * blockDim.x + threadIdx.x;
	int i, owner_tile, row_index, offset=0;

	if ( n < NB ) {
		for (i = 0 ; i < MB ; i++, A++) {
			owner_tile = perm[i] / global_MB;
			row_index  = perm[i] % global_MB;

			if (perm[i] != i + global_MB*K) {
				if ((owner_tile == SRC_M) || (owner_tile == DST_M)) {
					if (DST_M == K)
						A[i] = rows[offset+n];
					else
						A[row_index] = rows[offset+n];
					offset += NB;
				}
			}
		}
	}
}

void CUDA_zswp_insert_rows(int global_MB, int *perm,
                           cuDoubleComplex *A, cuDoubleComplex *rows,
                           int K, int SRC_M, int DST_M, int NB, int MB, int LDA,
                           cudaStream_t stream)
{
	dim3 grid( NB / CHAMELEON_CUDA_NTHREADS );
	dim3 threads( CHAMELEON_CUDA_NTHREADS );
	CUDA_zswp_insert_rows_kernel<<<grid, threads, 0, stream>>>
            (global_MB, perm, A, rows, K, SRC_M, DST_M, NB, MB, LDA);

	cudaError_t cures;
	cures = cudaStreamSynchronize(stream);
	if (cures)
        {
            const char *errormsg = cudaGetErrorString(cures);
            printf("Error in swp_extract_rows_gpu: %s\n", errormsg);
        }
}

__global__ void CUDA_zswp_extract_rows_kernel(int global_MB, int *perm,
                                              cuDoubleComplex *A,
                                              cuDoubleComplex *rows,
                                              int K, int SRC_M, int DST_M,
                                              int NB, int MB, int LDA)
{
    int n = blockIdx.x * blockDim.x + threadIdx.x;
    int i, owner_tile, row_index, offset=0;

    if ( n < NB ) {
        A = A + n * LDA;
        for (i = 0 ; i < MB ; i++) {
            owner_tile = perm[i] / global_MB;
            row_index  = perm[i] % global_MB;

            if (perm[i] != i + global_MB*K) {
                if ((owner_tile == SRC_M) || (owner_tile == DST_M)) {
                    cuDoubleComplex tmp;
                    if (SRC_M == DST_M && SRC_M == K)
                        tmp = A[row_index];
                    else if (SRC_M == K)
                        tmp = A[i];
                    else
                        tmp = A[row_index];
                    rows[n + offset] = tmp;
                    offset += NB;
                }
            }
        }
    }
}

void CUDA_zswp_extract_rows(int global_MB, int *perm,
                            cuDoubleComplex *A, cuDoubleComplex *rows,
                            int K, int SRC_M, int DST_M, int NB, int MB, int LDA,
                            cudaStream_t stream)
{
    dim3 grid( NB / CHAMELEON_CUDA_NTHREADS );
    dim3 threads( CHAMELEON_CUDA_NTHREADS );
    CUDA_zswp_extract_rows_kernel<<<grid, threads, 0, stream>>>
        (global_MB, perm, A, rows, K, SRC_M, DST_M, NB, MB, LDA);

    cudaError_t cures;
    cures = cudaStreamSynchronize(stream);
    if (cures)
    {
        const char *errormsg = cudaGetErrorString(cures);
        printf("Error in swp_extract_rows_gpu: %s\n", errormsg);
    }
}
