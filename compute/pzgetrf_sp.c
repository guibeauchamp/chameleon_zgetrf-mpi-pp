/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2017 Inria. All rights reserved.
 * @copyright (c) 2012-2017 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 * @file pzgetrf_sp.c
 *
 *  MORSE auxiliary routines
 *  MORSE is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver
 *
 * @author Omar Zenati
 * @author Terry Cojean
 * @date 2011-07-06
 * @precisions normal z -> s d c
 *
 **/
#include "control/common.h"

#define A(m,n) A,  m,  n

/***************************************************************************//**
 *  Parallel tile LU decomposition - dynamic scheduling
 **/
void morse_pzgetrf_sp(MORSE_desc_t *A,
		      double criteria, MORSE_data_handle_t *nb_pivot_handle,
                      MORSE_sequence_t *sequence, MORSE_request_t *request)
{
    MORSE_context_t *morse;
    MORSE_option_t options;

    int k, m, n;
    int tempkm, tempkn, tempmm,tempnn;
    int minmnt;
    int ldak, ldam;
    size_t ws_host   = 0;

    MORSE_Complex64_t zone  = (MORSE_Complex64_t) 1.0;
    MORSE_Complex64_t mzone = (MORSE_Complex64_t)-1.0;

    morse = morse_context_self();
    if (sequence->status != MORSE_SUCCESS)
        return;
    RUNTIME_options_init(&options, morse, sequence, request);

#ifdef CHAMELEON_USE_MAGMA
    if (0) /* Disable the workspace as long as it is is not used (See StarPU codelet) */
    {
        int nb = MORSE_IB; /* Approximate nb for simulation */
#if !defined(CHAMELEON_SIMULATION)
        nb = magma_get_zpotrf_nb(A->nb);
#endif
        ws_host = sizeof(MORSE_Complex64_t)*nb*nb;
    }
#endif
    RUNTIME_options_ws_alloc( &options, 0, ws_host );

    minmnt = (A->mt < A->nt)? A->mt : A->nt;
    for (k = 0; k < minmnt; k++) {
        tempkm = k == A->mt-1 ? A->m-k*A->mb : A->mb;
        tempkn = k == A->nt-1 ? A->n-k*A->nb : A->nb;
        ldak = BLKLDD(A, k);

        MORSE_TASK_zgetrf_sp(
            &options,
            tempkm, tempkn,
            A(k, k), criteria, nb_pivot_handle);
        /* TRSM_L */
        for (m = k+1; m < A->mt; m++) {
            tempmm = m == A->mt-1 ? A->m-m*A->mb : A->mb;
            ldam = BLKLDD(A, m);
            MORSE_TASK_ztrsm(
                &options,
                MorseRight, MorseUpper, MorseNoTrans, MorseNonUnit,
                tempkm, tempmm, A->mb,
                zone, A(k, k), ldak,
                      A(m, k), ldam);
        }

        /* TRSM_U */
        for (n = k+1; n < A->nt; n++) {
            tempnn = n == A->nt-1 ? A->n-n*A->nb : A->nb;
            MORSE_TASK_ztrsm(
                &options,
                MorseLeft, MorseLower, MorseNoTrans, MorseUnit,
                tempkm, tempnn, A->nb,
                zone, A(k, k), ldak,
                      A(k, n), ldak);
        }

        /* GEMM */
        for (m = k+1; m < A->mt; m++) {
            tempmm = m == A->mt-1 ? A->m-m*A->mb : A->mb;
            ldam = BLKLDD(A, m);
            for (n = k+1; n < A->nt; n++) {
                tempnn = n == A->nt-1 ? A->n-n*A->nb : A->nb;
                MORSE_TASK_zgemm(
		    &options,
		    MorseNoTrans, MorseNoTrans,
		    tempmm, tempnn, A->mb, A->mb,
		    mzone, A(m, k), ldam,
		           A(k, n), ldak,
		    zone,  A(m, n), ldam);
            }
        }
    }

    RUNTIME_options_ws_free(&options);
    RUNTIME_options_finalize(&options, morse);
}
