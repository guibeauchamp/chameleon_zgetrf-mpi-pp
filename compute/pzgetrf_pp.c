/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2017 Inria. All rights reserved.
 * @copyright (c) 2012-2017 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 *  @file pzgetrf.c
 *
 *  MORSE compute
 *  MORSE is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver,
 *  and INRIA Bordeaux Sud-Ouest
 *
 *  @author Omar Zenati
 *  @author Terry Cojean
 *  @date 2011-07-06
 *  @precisions normal z -> c d s
 *
 **/
#include "control/common.h"

#define A(m, n) A, m, n
#define Acopy(m, n) Acopy, m, n
#define IPIV_h(it) IPIV,it
#define perm_to_upper_h(it) perm_to_upper,it
#define perm_from_upper_h(it) perm_from_upper,it
#define IPIVh_p(it,index) (((int*)(IPIV->get_blkaddr(IPIV,it,0)))+index)
#define perm_to_upper_p(it, index) (((int*)(perm_from_upper->get_blkaddr(perm_from_upper,it,0)))+index)
#define perm_from_upper_p(it, index) (((int*)(perm_from_upper->get_blkaddr(perm_from_upper,it,0)))+index)



#define ALLOCATE_DYNAMIC_HANDLE(_handle_, _datatype_,_type_, _nbrow_,_nbcol_)\
    MORSE_data_handle_t *_handle_;\
    if (_nbrow_>0 && _nbcol_ > 0) { \
        MORSE_desc_t *desc;/*We should add a new descriptor creation function.*/\
        MORSE_Desc_Create(&desc, NULL, _datatype_, _nbrow_, _nbcol_, _nbrow_*_nbcol_ * sizeof(_type_),_nbrow_, _nbcol_, 0, 0,  _nbrow_, _nbcol_, _nbrow_, _nbcol_);\
        _handle_ = malloc(sizeof(MORSE_data_handle_t));/*FIXME ; Add an inderection?*/\
        _handle_->data_handle = RUNTIME_desc_getaddr(desc, 0, 0);\
        _handle_->ptr = desc->get_blkaddr(desc,0,0);\
    }

static inline void pivot_to_permutation(int diag_size, int height,
                                        int *IPIV, int *source, int *dest)
{
    int i;
    int pivoted, inverse;

    for(i=0; i < diag_size; i++)
        source[i] = dest[i] = i + height;

    for(i = 0; i < diag_size; i++) {
        int ip = IPIV[i]-1;
        assert( ip - height >= i );
        if ( ip-height > i ) {
            pivoted = source[i];

            if (ip-height < diag_size) {
                inverse = source[ip-height];
                source[ip-height] = pivoted;
            } else {
                inverse = ip;
                int j;
                for(j=0; j < diag_size; j++) {
                    if( dest[j] == ip ) {
                        inverse = j + height;
                        break;
                    }
                }
            }

            source[i] = inverse;

            pivoted -= height;
            inverse -= height;

            if (pivoted < diag_size) dest[pivoted] = ip;
            if (inverse < diag_size) dest[inverse] = i+height;
        }
    }
}

static void submit_swaps(MORSE_desc_t *A, int k, int n,
                         MORSE_desc_t *IPIV,
                         MORSE_desc_t *source, MORSE_desc_t *dest,
                         int *TILES_SRC, int *TILES_DST, MORSE_option_t *options)
{
    int tempk  = k * A->mb;
    int tempkmt = k * A->mt;
    int tempm  = A->m - tempk;
    int tempkm = k == A->mt-1 ? A->m-k*A->mb : A->mb;
    int tempkn = k == A->nt-1 ? A->n-k*A->nb : A->nb;
    int tempnn = n == A->nt-1 ? A->n-n*A->nb : A->nb;
    MORSE_desc_t *updatePanel = morse_desc_submatrix(A, tempk, n*A->nb, tempm, tempnn);

    if (options->swap_grain == MORSE_LINE_SWAPS) {
        int m;

        int nb_source = TILES_SRC[tempkmt];
        ALLOCATE_DYNAMIC_HANDLE(handle_upper_to_itself, MorseComplexDouble,MORSE_Complex64_t, nb_source , tempnn);
        if (nb_source > 0) {
            m = 0;
            int tempmm = m == A->mt-1 ? A->m-m*A->mb : A->mb; 
            //width of part of line to exchange (== width of the column of bloc).
            MORSE_TASK_zswp_extract_rows(options, nb_source, A->mb, k, k, k, tempmm, tempnn, source, A(k,n), handle_upper_to_itself);
        }

        for (m = 1 ; m < updatePanel->mt ; m++)
        {
            int tempmm = m == A->mt-1 ? A->m-m*A->mb : A->mb;
            nb_source = TILES_SRC[tempkmt+m];
            int nb_dest = TILES_DST[tempkmt+m];
            if (nb_source > 0 || nb_dest > 0) {
                /* ALLOC */
                ALLOCATE_DYNAMIC_HANDLE(handle_to_upper_tile,MorseComplexDouble,  MORSE_Complex64_t, nb_source , tempnn);
                ALLOCATE_DYNAMIC_HANDLE(handle_from_upper_tile, MorseComplexDouble, MORSE_Complex64_t, nb_dest , tempnn);
                /* TODO : make a proper allocation (need separate allocation 
                and mpi register of a single bloc with same  global tag intead of 
                allocating multiple matrix of 1 bloc with each different global tag)
                Still isn't freed, but wasn't before (wait until new alloc done).*/
                MORSE_TASK_zswp_extract_rows(options, nb_dest, A->mb, k, k, k+m, tempmm, tempnn, dest, A(k,n), handle_from_upper_tile);
                MORSE_TASK_zswp_extract_rows(options, nb_source, A->mb, k, k+m, k, tempmm, tempnn, source, A(k+m,n), handle_to_upper_tile);

                //Pour r�injecter ce que l'on a r�cup dans chacune
                MORSE_TASK_zswp_insert_rows(options, nb_source, A->mb, k, k+m, k, tempmm, tempnn, source, A(k,n), handle_to_upper_tile);
                MORSE_TASK_zswp_insert_rows(options, nb_dest, A->mb, k, k, k+m, tempmm, tempnn, dest, A(k+m,n), handle_from_upper_tile);

                /* TODO: make these evolve to free the handle and its data */
                MORSE_TASK_flush_handle(options, handle_to_upper_tile);
                MORSE_TASK_flush_handle(options, handle_from_upper_tile);
            }
        }

        nb_source = TILES_SRC[tempkmt];
        if (nb_source > 0) {
            m = 0;
            int tempmm = m == A->mt-1 ? A->m-m*A->mb : A->mb;
            MORSE_TASK_zswp_insert_rows(options, nb_source, A->mb, k, k, k, tempmm, tempnn, source, A(k,n), handle_upper_to_itself);

            MORSE_TASK_flush_handle(options, handle_upper_to_itself);
        }
    } else if (options->swap_grain == MORSE_TILE_SWAPS) {
        int m;

        for (m = 0 ; m < updatePanel->mt ; m++)
        {
            if (TILES_SRC[tempkmt+m] >= 1) {
                MORSE_TASK_zswpp(options,
                                   updatePanel, A(k, n), A(k+m, n),
                                   1, tempkm,
                                   IPIV, 1);
            }
        }
 /*   } else if (options->swap_grain == MORSE_COLUMN_SWAPS) {
        MORSE_TASK_zlaswp_ontile(options,
                                 updatePanel, A(k, n), 1,
                                 chameleon_min(tempkm, tempkn), IPIV_handle, 1);
 */
    } else {
        /*
         * Using something other than a DSD matrix without SWAPs produces
         * unstable behaviour. Remove this assert with caution.
         */
        assert(options->dsd_matrix == 1);
    }
    /* TODO: free updatePanel asynchronously */
}

static void compute_swap_amount(MORSE_option_t *options, int k,
                                MORSE_desc_t *A, MORSE_desc_t *IPIV,/*FIXME : USE DESC iINSTEAD*/
                                 MORSE_desc_t *perm_from_upper,
                                 MORSE_desc_t *perm_to_upper,
                                int* nbswaps_from_upper, int* nbswaps_to_upper)
{
    int tempk  = k * A->mb;
    int tempkm = k == A->mt-1 ? A->m-k*A->mb : A->mb;
    int tempkn = k == A->nt-1 ? A->n-k*A->nb : A->nb;
    int minkmn = ((tempkm < tempkn)?tempkm:tempkn);

    if (options->swap_grain == MORSE_LINE_SWAPS) {
        int i;
        pivot_to_permutation(tempkm, k*A->mb, IPIVh_p(k,0),
                             perm_from_upper_p(k,0), perm_to_upper_p(k,0));

        for (i=0 ; i<tempkm ; i++) {
            int index = (*perm_from_upper_p(k,i)-tempk)/((int)A->mb);
            if (*perm_from_upper_p(k,i) != tempk+i) {
                nbswaps_from_upper[A->mt*k+index] += 1;
            }
            index = (*perm_to_upper_p(k,i)-tempk)/((int)A->mb);
            if (*perm_to_upper_p(k,i) != tempk+i) {
                nbswaps_to_upper[A->mt*k+index] += 1;
            }
        }
    } else if (options->swap_grain == MORSE_TILE_SWAPS) {
        int i;
        for (i=0 ; i<tempkm ; i++) {
            int index = (*IPIVh_p(k,i)-tempk-1)/((int)A->mb);
            if (*IPIVh_p(k,i) != tempk+i+1)
                nbswaps_from_upper[A->mt*k+index] += 1;
        }
    }
}

static void factorize_panel(int rectil, MORSE_option_t *options,
                            int k, MORSE_desc_t *A, MORSE_desc_t * IPIV,
                            MORSE_desc_t *workspace)
 {
     int tempk  = k * A->mb;
     int tempkm = k == A->mt-1 ? A->m-k*A->mb : A->mb;
     int tempkn = k == A->nt-1 ? A->n-k*A->nb : A->nb;
     int minkmn = ((tempkm < tempkn)?tempkm:tempkn);

/*     if (rectil)
     {
         MORSE_TASK_zgetrf_rectil(
             options,
             morse_desc_submatrix(A, tempk, k*A->nb, A->m - tempk, tempkn),
             IPIV);
     }
     else*/
     {
         int h, m, d, r;
         int height, dmax, pui;
         int topid, bottomid;
         for(h = 0; h <= minkmn; h++) {
            /* Apply last update and extract local max */
            for (m = 0; m < A->mt; m++) {
                int tempmm = m == A->mt-1 ? A->m-m*A->mb : A->mb;
                MORSE_TASK_zgetrf_extract_max(options,
                                              k,tempmm,tempkn,h,A->nb,
                                              A(m,k),
                                              workspace);
            }
            if(h != minkmn) {
                /* Reduce max */
                height = A->mt-k;
                dmax = (int)ceil( log ( height ) / log(2.) );
                /* if(h==0) */
                /*   printf("k=%d, height=%d, dmax=%d\n",k,height,dmax); */
                for(d = 0; d < dmax; d++){
                    pui = (1 << d);
                    for(r = 0; (r+pui) < height; r+=pui*2) {
                        /* if(h == 0) */
                        /*   printf("reduction entre %d et %d a %d\n",r+k,r+k+pui,k); */
                        topid = r + k;
                        bottomid = r + k + pui;
                        MORSE_TASK_zgetrf_compare_workspace(options,
                                                            tempkn, h, A->nb,
                                                            workspace,topid, bottomid);
                    }
                }
                /* Broadcast global max */
                for (m = k+1; m < A->mt; m++) {
                    topid =  m;
                    bottomid = k;
                    MORSE_TASK_zgetrf_copy_workspace(options,
                                                     tempkn, h, A->nb,
                                                     workspace,topid, bottomid);

                }
                /* Save pivot */
                MORSE_TASK_zgetrf_save_pivots(options, h, IPIV, workspace, k);
            }
        }

     }

 }

void morse_pzgetrf_pp(MORSE_desc_t *A,
                      MORSE_desc_t *IPIV,
                      MORSE_desc_t *perm_from_upper,
                      MORSE_desc_t *perm_to_upper,
                      int *nbswaps_from_upper, int *nbswaps_to_upper,
                      MORSE_desc_t *workspace,
                      MORSE_sequence_t *sequence,
                      MORSE_request_t *request)
{
    MORSE_context_t *morse;
    MORSE_option_t options;
    int k, m, n;
    int tempkm, tempkn, tempmm, tempnn, minmnt;
    int ldak, ldam;
    int rectil = 0, ncores = -1;
    int *cores = NULL;

    MORSE_Complex64_t zone  = (MORSE_Complex64_t) 1.0;
    MORSE_Complex64_t mzone = (MORSE_Complex64_t)-1.0;

    morse = morse_context_self();
    if (sequence->status != MORSE_SUCCESS)
        return;

    RUNTIME_options_init( &options, morse, sequence, request );

    minmnt = (A->mt < A->nt)? A->mt : A->nt;

    /* Treat the related morse context options */
    // if (MORSE_RECTIL_PANEL)
    // {
    //     rectil = 1;
    //     /* TODO: Fix it. This doesn't work with GPUs, it's too simple:
    //      * we need to fetch a CPU list from the Runtime, and put ids from this list
    //      * To do this, the best thing would be to push this into the runtime alloc function
    //      */
    //     if (MORSE_PANEL_THREADS > 0)
    //     {
    //         ncores = MORSE_PANEL_THREADS;
    //         cores = (int*)malloc(ncores * sizeof(int));
    //         for (k=0; k<ncores; k++)
    //             cores[k] = k;
    //     }
    //     RUNTIME_alloc_threads(&options, ncores, cores);
    //     free(cores);
    //     CORE_zgetrf_rectil_init();
    // }

    if (options.swap_grain == MORSE_LINE_SWAPS) {
        assert(A->m%A->mb == 0);
        assert(A->n%A->nb == 0);
    }

    /***********************/
    /* Panel factorization */
    /***********************/
    factorize_panel(rectil, &options, 0, A,
                    IPIV, workspace);

    for (k = 0; k < minmnt; k++) {
        tempkm = k == A->mt-1 ? A->m-k*A->mb : A->mb;
        tempkn = k == A->nt-1 ? A->n-k*A->nb : A->nb;
        ldak = BLKLDD(A, k);

        /***************************/
        /* Compute amount of swaps */
        /***************************/
        //  void * IPIV_addr = RUNTIME_desc_getaddr(IPIV, k, 0); 
        //  MORSE_data_handle_t IPIV_handle =  {&IPIV_addr,NULL};//IPIV->get_blkaddr(IPIV,k,0)
        // // //DONE : can't work with MPI. Could create a task and wait it. 
        //      TODO : Add line swap support.
        //  RUNTIME_data_acquire(&IPIV_handle); 
        //  compute_swap_amount(&options, k, A, IPIV, perm_from_upper, perm_to_upper,
        //                      nbswaps_from_upper, nbswaps_to_upper);
        //  RUNTIME_data_release(&IPIV_handle);
        MORSE_TASK_zgetrf_compute_swap_sync(&options, k, A, IPIV, perm_from_upper, 
                                            perm_to_upper,nbswaps_from_upper,
                                            nbswaps_to_upper,k);

        /* TRSM_U */
        for (n = k+1; n < A->nt; n++) {
            tempnn = ((n == A->nt-1)? A->n-n*A->nb : A->nb);

            submit_swaps(A, k, n, IPIV, perm_from_upper, perm_to_upper, nbswaps_from_upper, nbswaps_to_upper, &options);

            MORSE_TASK_ztrsm(
                &options,
                MorseLeft, MorseLower, MorseNoTrans, MorseUnit,
                tempkm, tempnn, tempnn,
                zone, A(k, k), ldak,
                A(k, n), ldak);

            /* GEMM */
            for (m = k+1; m < A->mt; m++) {
                tempmm = m == A->mt-1 ? A->m-m*A->mb : A->mb;
                ldam = BLKLDD(A, m);

                MORSE_TASK_zgemm(
                    &options,
                    MorseNoTrans, MorseNoTrans,
                    tempmm, tempnn, tempkm, tempkn,
                    mzone, A(m, k), ldam,
                    A(k, n), ldak,
                    zone,  A(m, n), ldam);
            }

            if (n==k+1)
            {
                factorize_panel(rectil, &options, n, A,
                                IPIV, workspace);
            }
        }

        /* Backward swaps */
        for (n = 0 ; n < k ; n++) {
            submit_swaps(A, k, n, IPIV, perm_from_upper, perm_to_upper,
                         nbswaps_from_upper, nbswaps_to_upper, &options);
        }
    }

    /* if (MORSE_RECTIL_PANEL) */
    /* { */
    /*     RUNTIME_unalloc_threads(&options); */
    /* } */
    RUNTIME_options_finalize( &options, morse );
}
