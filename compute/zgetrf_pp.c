/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2017 Inria. All rights reserved.
 * @copyright (c) 2012-2017 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 * @file zgetrf_pp.c
 *
 *  MORSE computational routines
 *  MORSE is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver
 *
 * @version 2.6.0
 * @author Omar Zenati
 * @author Terry Cojean
 * @date 2011-07-06
 *
 * @precisions normal z -> s d c
 *
 **/
#include "control/common.h"

/***************************************************************************//**
 *
 * @ingroup MORSE_Complex64_t
 *
 *  MORSE_zgetrf_pp - Computes an LU factorization of a general M-by-N matrix A
 *  using the tile LU algorithm with partial pivoting with row interchanges.
 *
 *******************************************************************************
 *
 * @param[in] M
 *          The number of rows of the matrix A. M >= 0.
 *
 * @param[in] N
 *          The number of columns of the matrix A. N >= 0.
 *
 * @param[in,out] A
 *          On entry, the M-by-N matrix to be factored.
 *          On exit, the tile factors L and U from the factorization.
 *
 * @param[in] LDA
 *          The leading dimension of the array A. LDA >= max(1,M).
 *
 * @param[out] IPIV
 *          The pivot indices that define the permutations (not equivalent to LAPACK).
 *
 *******************************************************************************
 *
 * @return
 *          \retval MORSE_SUCCESS successful exit
 *          \retval <0 if -i, the i-th argument had an illegal value
 *          \retval >0 if i, U(i,i) is exactly zero. The factorization has been completed,
 *               but the factor U is exactly singular, and division by zero will occur
 *               if it is used to solve a system of equations.
 *
 *******************************************************************************
 *
 * @sa MORSE_zgetrf_Tile
 * @sa MORSE_zgetrf_Tile_Async
 * @sa MORSE_cgetrf
 * @sa MORSE_dgetrf
 * @sa MORSE_sgetrf
 * @sa MORSE_zgetrs
 *
 ******************************************************************************/
/* TODO: Fix the untiled version !! */
int MORSE_zgetrf_pp(int M, int N,
                    MORSE_Complex64_t *A, int LDA, int *IPIV)
{
    int NB;
    int status;
    MORSE_desc_t descA;
    MORSE_context_t *morse;
    MORSE_sequence_t *sequence = NULL;
    MORSE_request_t *request = MORSE_REQUEST_INITIALIZER;

    morse = morse_context_self();
    if (morse == NULL) {
        morse_fatal_error("MORSE_zgetrf_pp", "MORSE not initialized");
        return MORSE_ERR_NOT_INITIALIZED;
    }
    /* Check input arguments */
    if (M < 0) {
        morse_error("MORSE_zgetrf_pp", "illegal value of M");
        return -1;
    }
    if (N < 0) {
        morse_error("MORSE_zgetrf_pp", "illegal value of N");
        return -2;
    }
    if (LDA < chameleon_max(1, M)) {
        morse_error("MORSE_zgetrf_pp", "illegal value of LDA");
        return -4;
    }
    /* Quick return */
    if (chameleon_min(M, N) == 0)
        return MORSE_SUCCESS;

    /* Tune NB & IB depending on M, N & NRHS; Set NBNBSIZE */
    status = morse_tune(MORSE_FUNC_ZGESV, M, N, 0);
    if (status != MORSE_SUCCESS) {
        morse_error("MORSE_zgetrf_pp", "morse_tune() failed");
        return status;
    }

    morse_sequence_create(morse, &sequence);


    /* Set NT & NTRHS */
    NB   = MORSE_NB;

/*    if ( MORSE_TRANSLATION == MORSE_OUTOFPLACE ) {*/
        morse_zooplap2tile( descA, A, NB, NB, LDA, N, 0, 0, M, N, sequence, request,
                             morse_desc_mat_free(&(descA)) );
/*    } else {*/
/*        morse_ziplap2tile( descA, A, NB, NB, LDA, N, 0, 0, M, N,*/
/*                            sequence, &request);*/
/*    }*/
	int minmn = (M < N) ? M : N;
	int nb_pivots = minmn / NB + ((minmn % NB) ? 1 : 0);
	int minmnt = nb_pivots * NB;

        MORSE_desc_t * IPIV_h = NULL;
        MORSE_desc_t * swaps_from_upper_h = NULL;
        MORSE_desc_t * swaps_to_upper_h = NULL;

        int nb_nodes= 1;


        MORSE_Desc_Create(&IPIV_h, IPIV, MorseInteger, 1, descA.nb, descA.nb,nb_pivots, descA.nb, 0, 0, nb_pivots, descA.nb,nb_nodes , 1);
        MORSE_Desc_Create(&swaps_from_upper_h, NULL, MorseInteger, 1, descA.nb, descA.nb,nb_pivots, descA.nb, 0, 0, nb_pivots, descA.nb,nb_nodes , 1);
        MORSE_Desc_Create(&swaps_to_upper_h, NULL, MorseInteger, 1, descA.nb, descA.nb,nb_pivots, descA.nb, 0, 0, nb_pivots, descA.nb,nb_nodes , 1);



        int *nbswaps_from_upper = (int*)calloc(minmn, sizeof(int));
        int *nbswaps_to_upper = (int*)calloc(minmn, sizeof(int));


        /*
        int *swaps_from_upper = (int*)calloc(minmnt * descA.nb, sizeof(int));
        int *swaps_to_upper = (int*)calloc(minmnt * descA.nb, sizeof(int));

        MORSE_data_handle_t IPIV_h[nb_pivots];
        MORSE_data_handle_t swaps_from_upper_h[nb_pivots];
        MORSE_data_handle_t swaps_to_upper_h[nb_pivots];

        int i;
        for(i = 0; i < nb_pivots; i++) {
            RUNTIME_data_alloc(IPIV_h + i);
            RUNTIME_vector_data_register(IPIV_h + i, 0, (uintptr_t)(IPIV + i * descA.nb), descA.nb, sizeof(int));

            RUNTIME_data_alloc(swaps_from_upper_h + i);
            RUNTIME_vector_data_register(swaps_from_upper_h + i, 0,
                                         (uintptr_t)(swaps_from_upper + i * descA.nb),
                                         descA.nb, sizeof(int));

            RUNTIME_data_alloc(swaps_to_upper_h + i);
            RUNTIME_vector_data_register(swaps_to_upper_h + i, 0,
                                         (uintptr_t)(swaps_to_upper+ i * descA.nb),
                                         descA.nb, sizeof(int));
        }*/


        
        int nb_workspace = descA.mt;
        int workspace_size = NB * 2 + 1;


        MORSE_desc_t * max_workspace = NULL;

        MORSE_Desc_Create(&max_workspace, NULL, MorseComplexDouble, 1, workspace_size, workspace_size,nb_workspace, workspace_size, 0, 0,  nb_workspace, workspace_size, nb_nodes , 1);
/*
        MORSE_data_handle_t *max_workspace = malloc(sizeof(MORSE_data_handle_t) * nb_workspace);
        MORSE_Complex64_t **workspaces = malloc(sizeof(MORSE_Complex64_t *) * nb_workspace);

        for(i = 0; i < nb_workspace; i++) {
            workspaces[i] = malloc(sizeof(MORSE_Complex64_t) * workspace_size);
            RUNTIME_vector_data_register(max_workspace + i, 0, (uintptr_t)(workspaces[i]),
                                         workspace_size, sizeof(MORSE_Complex64_t));
        }
*/
    /* Call the tile interface */
	MORSE_zgetrf_pp_Tile_Async(&descA, IPIV_h, swaps_from_upper_h, swaps_to_upper_h,
                                   nbswaps_from_upper, nbswaps_to_upper, max_workspace,
                                   sequence, request);

/*    if ( MORSE_TRANSLATION == MORSE_OUTOFPLACE ) {*/
        morse_zooptile2lap(descA, A, NB, NB, LDA, N,  sequence, request);
        RUNTIME_barrier(morse);
        RUNTIME_desc_getoncpu(&descA);
        morse_desc_mat_free(&descA);
/*    } else {*/
/*        morse_ziptile2lap( descA, A, NB, NB, LDA, N,  sequence, &request);*/
/*        RUNTIME_barrier(morse);*/
/*    }*/
/*
        for (i = 0 ; i < nb_pivots ; i++)  {
            RUNTIME_data_unregister(IPIV_h+i);
            RUNTIME_data_unregister(swaps_from_upper_h+i);
            RUNTIME_data_unregister(swaps_to_upper_h+i);
        }

        free(swaps_from_upper);
        free(swaps_to_upper);
        free(max_workspace);
        for(i = 0; i < nb_workspace; i++) {
            free(workspaces[i]);
        }
        free(workspaces);
*/
        free(nbswaps_from_upper);
        free(nbswaps_to_upper);


        MORSE_Desc_Destroy(&IPIV_h );
        MORSE_Desc_Destroy(&swaps_from_upper_h );
        MORSE_Desc_Destroy(&swaps_to_upper_h );
        MORSE_Desc_Destroy(&max_workspace);       

    return status;
}

/***************************************************************************//**
 *
 * @ingroup MORSE_Complex64_t_Tile
 *
 *  MORSE_zgetrf_pp_Tile - Computes the tile LU factorization of a matrix.
 *  Tile equivalent of MORSE_zgetrf().
 *  Operates on matrices stored by tiles.
 *  All matrices are passed through descriptors.
 *  All dimensions are taken from the descriptors.
 *
 *******************************************************************************
 *
 * @param[in,out] A
 *          On entry, the M-by-N matrix to be factored.
 *          On exit, the tile factors L and U from the factorization.
 *
 * @param[out] IPIV
 *          The pivot indices that define the permutations (not equivalent to LAPACK).
 *
 *******************************************************************************
 *
 * @return
 *          \retval MORSE_SUCCESS successful exit
 *          \retval >0 if i, U(i,i) is exactly zero. The factorization has been completed,
 *               but the factor U is exactly singular, and division by zero will occur
 *               if it is used to solve a system of equations.
 *
 *******************************************************************************
 *
 * @sa MORSE_zgetrf
 * @sa MORSE_zgetrf_Tile_Async
 * @sa MORSE_cgetrf_Tile
 * @sa MORSE_dgetrf_Tile
 * @sa MORSE_sgetrf_Tile
 * @sa MORSE_zgetrs_Tile
 *
 ******************************************************************************/
int MORSE_zgetrf_pp_Tile(MORSE_desc_t *A, int *IPIV)
{
    /* FIXME: Actually use correct nb/mb/min(...) scheme for computing pivot amount */
    int minmn = chameleon_min(A->m, A->n);
    int minmnt = (A->mt < A->nt) ? A->mt : A->nt;
    int nb_pivots = minmn / A->nb + ((minmn % A->nb) ? 1 : 0);
    int status;
    int i;
    MORSE_context_t *morse = morse_context_self();
    MORSE_sequence_t *sequence = NULL;
    MORSE_request_t request = MORSE_REQUEST_INITIALIZER;

    int nb_workspace = A->mt;
    int workspace_size = A->nb * 2 + 1;

    MORSE_desc_t * IPIV_h = NULL;
    MORSE_desc_t * swaps_from_upper_h = NULL;
    MORSE_desc_t * swaps_to_upper_h = NULL;
    MORSE_desc_t * max_workspace = NULL;

    int nb_nodes= A->p * A->q;

    MORSE_Desc_Create(&IPIV_h, IPIV, MorseInteger, 1, A->nb, A->nb,nb_pivots, A->nb, 0, 0, nb_pivots, A->nb,nb_nodes , 1);
    MORSE_Desc_Create(&swaps_from_upper_h, NULL, MorseInteger, 1, A->nb, A->nb,nb_pivots, A->nb, 0, 0, nb_pivots, A->nb,nb_nodes , 1);
    MORSE_Desc_Create(&swaps_to_upper_h, NULL, MorseInteger, 1, A->nb, A->nb,nb_pivots, A->nb, 0, 0, nb_pivots, A->nb,nb_nodes , 1);
    MORSE_Desc_Create(&max_workspace, NULL, MorseComplexDouble, 1, workspace_size, workspace_size ,nb_workspace, workspace_size, 0, 0,  nb_workspace, workspace_size, nb_nodes , 1);

    int *nbswaps_from_upper = (int*)calloc(nb_pivots * A->mt, sizeof(int));
    int *nbswaps_to_upper = (int*)calloc(nb_pivots * A->mt, sizeof(int));

/*
    int *swaps_from_upper = (int*)calloc(chameleon_max(A->m, A->n), sizeof(int));
    int *swaps_to_upper = (int*)calloc(chameleon_max(A->m, A->n), sizeof(int));
    MORSE_data_handle_t IPIV_h[nb_pivots];
    MORSE_data_handle_t swaps_from_upper_h[nb_pivots];
    MORSE_data_handle_t swaps_to_upper_h[nb_pivots];


    MORSE_data_handle_t *max_workspace = malloc(sizeof(MORSE_data_handle_t) * nb_workspace);
    MORSE_Complex64_t *workspaces = malloc(sizeof(MORSE_Complex64_t) * nb_workspace * workspace_size);


    for(i = 0; i < nb_workspace; i++) {
        RUNTIME_data_alloc(max_workspace + i);
        RUNTIME_vector_data_register(max_workspace + i, 0,
                                     (uintptr_t)(workspaces + i * workspace_size),
                                     workspace_size, sizeof(MORSE_Complex64_t));
    }

    for(i = 0; i < nb_pivots; i++) {
        int tempin = i == A->nt-1 ? A->n-i*A->nb : A->nb;
        RUNTIME_data_alloc(IPIV_h + i);
        RUNTIME_vector_data_register(IPIV_h + i, 0,
                                     (uintptr_t)(IPIV + i * A->nb),
                                     tempin, sizeof(int));

        RUNTIME_data_alloc(swaps_from_upper_h + i);
        RUNTIME_vector_data_register(swaps_from_upper_h + i, 0,
                                     (uintptr_t)(swaps_from_upper + i * A->nb),
                                     tempin, sizeof(int));

        RUNTIME_data_alloc(swaps_to_upper_h + i);
        RUNTIME_vector_data_register(swaps_to_upper_h + i, 0,
                                     (uintptr_t)(swaps_to_upper+ i * A->nb),
                                     tempin, sizeof(int));
    }
*/

    morse_sequence_create(morse, &sequence);
    MORSE_zgetrf_pp_Tile_Async(A, IPIV_h, swaps_from_upper_h, swaps_to_upper_h,
                               nbswaps_from_upper, nbswaps_to_upper,
                               max_workspace, sequence, &request);
    morse_sequence_wait(morse, sequence);
    RUNTIME_desc_getoncpu(A);

    MORSE_Desc_Destroy(&IPIV_h );
    MORSE_Desc_Destroy(&swaps_from_upper_h );
    MORSE_Desc_Destroy(&swaps_to_upper_h );
    MORSE_Desc_Destroy(&max_workspace);      

/*
    for (i = 0 ; i < nb_pivots ; i++)  {
        RUNTIME_data_unregister(IPIV_h+i);
        RUNTIME_data_free(IPIV_h+i);

        RUNTIME_data_unregister(swaps_from_upper_h+i);
        RUNTIME_data_free(swaps_from_upper_h+i);

        RUNTIME_data_unregister(swaps_to_upper_h+i);
        RUNTIME_data_free(swaps_to_upper_h+i);
    }

    free(swaps_from_upper);
    free(swaps_to_upper);

    for(i = 0; i < nb_workspace; i++) {
        RUNTIME_data_unregister(max_workspace+i);
        RUNTIME_data_free(max_workspace+i);
    }
    free(max_workspace);
    free(workspaces);
*/ 

    free(nbswaps_from_upper);
    free(nbswaps_to_upper);

    status = sequence->status;
    morse_sequence_destroy(morse, sequence);
    return status;
}

/***************************************************************************//**
 *
 * @ingroup MORSE_Complex64_t_Tile_Async
 *
 *  MORSE_zgetrf_pp_Tile_Async - Computes the tile LU factorization of a matrix.
 *  Non-blocking equivalent of MORSE_zgetrf_Tile().
 *  May return before the computation is finished.
 *  Allows for pipelining of operations at runtime.
 *
 *******************************************************************************
 *
 * @param[in] sequence
 *          Identifies the sequence of function calls that this call belongs to
 *          (for completion checks and exception handling purposes).
 *
 * @param[out] request
 *          Identifies this function call (for exception handling purposes).
 *
 *******************************************************************************
 *
 * @sa MORSE_zgetrf
 * @sa MORSE_zgetrf_Tile
 * @sa MORSE_cgetrf_Tile_Async
 * @sa MORSE_dgetrf_Tile_Async
 * @sa MORSE_sgetrf_Tile_Async
 * @sa MORSE_zgetrs_Tile_Async
 *
 ******************************************************************************/
int MORSE_zgetrf_pp_Tile_Async(MORSE_desc_t *A,
                               MORSE_desc_t *IPIV_h,
                               MORSE_desc_t *swaps_from_upper_h,
                               MORSE_desc_t *swaps_to_upper_h,
                               int *nbswaps_from_upper,
                               int *nbswaps_to_upper,
                               MORSE_desc_t *workspace_h,
                               MORSE_sequence_t *sequence,
                               MORSE_request_t *request)
{
     MORSE_context_t *morse;

    morse = morse_context_self();
    if (morse == NULL) {
        morse_fatal_error("MORSE_zgetrf_pp_Tile", "MORSE not initialized");
        return MORSE_ERR_NOT_INITIALIZED;
    }
    if (sequence == NULL) {
        morse_fatal_error("MORSE_zgetrf_pp_Tile", "NULL sequence");
        return MORSE_ERR_UNALLOCATED;
    }
    if (request == NULL) {
        morse_fatal_error("MORSE_zgetrf_pp_Tile", "NULL request");
        return MORSE_ERR_UNALLOCATED;
    }
    /* Check sequence status */
    if (sequence->status == MORSE_SUCCESS)
        request->status = MORSE_SUCCESS;
    else
        return morse_request_fail(sequence, request, MORSE_ERR_SEQUENCE_FLUSHED);

    /* Check descriptors for correctness */
    if (morse_desc_check(A) != MORSE_SUCCESS) {
        morse_error("MORSE_zgetrf_pp_Tile", "invalid first descriptor");
        return morse_request_fail(sequence, request, MORSE_ERR_ILLEGAL_VALUE);
    }

    /* Check input arguments */
    if (A->nb != A->mb) {
        morse_error("MORSE_zgetrf_pp_Tile", "only square tiles supported");
        return morse_request_fail(sequence, request, MORSE_ERR_ILLEGAL_VALUE);
    }

    morse_pzgetrf_pp(A, IPIV_h, swaps_from_upper_h, swaps_to_upper_h,
                     nbswaps_from_upper, nbswaps_to_upper, workspace_h,
                     sequence, request);

    return MORSE_SUCCESS;
}
