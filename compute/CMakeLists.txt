###
#
# @copyright (c) 2009-2014 The University of Tennessee and The University
#                          of Tennessee Research Foundation.
#                          All rights reserved.
# @copyright (c) 2012-2016 Inria. All rights reserved.
# @copyright (c) 2012-2014 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
#
###
#
#  @file CMakeLists.txt
#
#  @project MORSE
#  MORSE is a software package provided by:
#     Inria Bordeaux - Sud-Ouest,
#     Univ. of Tennessee,
#     King Abdullah Univesity of Science and Technology
#     Univ. of California Berkeley,
#     Univ. of Colorado Denver.
#
#  @version 0.9.0
#  @author Cedric Castagnede
#  @author Emmanuel Agullo
#  @author Mathieu Faverge
#  @date 13-07-2012
#
###

if ( CHAMELEON_USE_CUDA )
  set( CHAMELEON_COPY_DIAG ON )
else()
  option(CHAMELEON_COPY_DIAG
    "This options enables the duplication of the diagonal tiles in some algorithm to avoid anti-dependencies on lower/upper triangular parts (Might be useful to StarPU)" ON)
endif()

if ( CHAMELEON_SCHED_QUARK )
  # No need for those extra diagonale tiles
  set( CHAMELEON_COPY_DIAG OFF )
endif()

mark_as_advanced(CHAMELEON_COPY_DIAG)

if (CHAMELEON_COPY_DIAG)
  add_definitions(-DCHAMELEON_COPY_DIAG)
endif()

# Define the list of sources
# --------------------------

set(flags_to_add "")
foreach(_prec ${CHAMELEON_PRECISION})
    set(flags_to_add "${flags_to_add} -DPRECISION_${_prec}")
endforeach()
set_source_files_properties(../control/tile.c PROPERTIES COMPILE_FLAGS "${flags_to_add}")

# Generate the morse sources for all possible precisions
# ------------------------------------------------------
set(CHAMELEON_SRCS_GENERATED "")
set(ZSRC
    ##################
    # BLAS 3
    ##################
    pzgemm.c
    pzhemm.c
    pzher2k.c
    pzherk.c
    pzsymm.c
    pzsyr2k.c
    pzsyrk.c
    pztrmm.c
    pztrsm.c
    pztrsmpl.c
    pztradd.c
    pzlascal.c
    ###
    zgeadd.c
    zlascal.c
    zgemm.c
    zhemm.c
    zher2k.c
    zherk.c
    zsymm.c
    zsyr2k.c
    zsyrk.c
    ztradd.c
    ztrmm.c
    ztrsm.c
    ztrsmpl.c
    ##################
    # LAPACK
    ##################
    pzgelqf.c
    pzgelqfrh.c
    pzgeqrf.c
    pzgeqrfrh.c
    pzgetrf_incpiv.c
    pzgetrf_nopiv.c
    pzgetrf_sp.c
    pzgetrf_pp.c
    pzlacpy.c
    pzlange.c
    pzlanhe.c
    pzlansy.c
    pzlantr.c
    pzlaset2.c
    pzlaset.c
    pzlauum.c
    pzplghe.c
    pzplgsy.c
    pzplrnt.c
    pzpotrf.c
    pzsytrf.c
    pztrtri.c
    pzpotrimm.c
    pzunglq.c
    pzunglqrh.c
    pzungqr.c
    pzungqrrh.c
    pzunmlq.c
    pzunmlqrh.c
    pzunmqr.c
    pzunmqrrh.c
    pztpgqrt.c
    pztpqrt.c
    ###
    zgels.c
    zgelqf.c
    zgelqs.c
    zgeqrf.c
    zgeqrs.c
    #zgesv.c
    zgesv_incpiv.c
    zgesv_nopiv.c
    #zgetrf.c
    zgetrf_incpiv.c
    zgetrf_nopiv.c
    zgetrf_sp.c
    zgetrf_pp.c
    zgetrs_incpiv.c
    zgetrs_nopiv.c
    zlacpy.c
    zlange.c
    zlanhe.c
    zlansy.c
    zlantr.c
    zlaset.c
    zlauum.c
    zplghe.c
    zplgsy.c
    zplrnt.c
    zposv.c
    zsysv.c
    zpotrf.c
    zsytrf.c
    zpotri.c
    zpotrimm.c
    zpotrs.c
    zsytrs.c
    ztrtri.c
    zunglq.c
    zungqr.c
    zunmlq.c
    zunmqr.c
    ztpgqrt.c
    ztpqrt.c
    ##################
    # MIXED PRECISION
    ##################
    pzlag2c.c
    ###
    #zcgels.c
    #zcgesv.c
    #zcposv.c
    #zcungesv.c
    ##################
    # OTHERS
    ##################
    pztile2band.c
    #pzgebrd_gb2bd.c
    pzgebrd_ge2gb.c
    #pzgetmi2.c
    #pzgetrf_reclap.c
    #pzgetrf_rectil.c
    #pzhegst.c
    #pzherbt.c
    #pzhetrd_hb2ht.c
    pzhetrd_he2hb.c
    #pzlarft_blgtrd.c
    #pzlaswp.c
    #pzlaswpc.c
    #pztrsmrv.c
    #pzunmqr_blgtrd.c
    #########################
    #zgebrd.c
    #zgecfi.c
    #zgecfi2.c
    zgesvd.c
    #zgetmi.c
    #zgetri.c
    #zgetrs.c
    #zheev.c
    zheevd.c
    #zhegst.c
    #zhegv.c
    #zhegvd.c
    zhetrd.c
    #zlaswp.c
    #zlaswpc.c
    #ztrsmrv.c
    ##################
    # CONTROL
    ##################
    #pzshift.c
    #pzpack.c
    pztile.c
    ztile.c
    ##################
    # BUILD
    ##################
    zbuild.c
    pzbuild.c
)

precisions_rules_py(CHAMELEON_SRCS_GENERATED "${ZSRC}"
                    PRECISIONS "${CHAMELEON_PRECISION}")

set(CONTROL_SRCS_GENERATED "")

set(CHAMELEON_SRCS
    ${CHAMELEON_SRCS_GENERATED}
   )

# Compile step
# ------------
add_library(chameleon ${CHAMELEON_SRCS})
target_link_libraries(chameleon chameleon_control)
list(INSERT CHAMELEON_DEP 0 -lchameleon_control)
if(CHAMELEON_SCHED_STARPU)
  target_link_libraries(chameleon chameleon_starpu)
  list(INSERT CHAMELEON_DEP 0 -lchameleon_starpu)
elseif(CHAMELEON_SCHED_PARSEC)
  target_link_libraries(chameleon chameleon_parsec)
  list(INSERT CHAMELEON_DEP 0 -lchameleon_parsec)
elseif(CHAMELEON_SCHED_QUARK)
  target_link_libraries(chameleon chameleon_quark)
  list(INSERT CHAMELEON_DEP 0 -lchameleon_quark)
endif()
if (NOT CHAMELEON_SIMULATION)
    target_link_libraries(chameleon coreblas)
endif()
list(INSERT CHAMELEON_DEP 0 -lchameleon)

add_dependencies(chameleon
  chameleon_control
  chameleon_include
  coreblas_include
  control_include
)
if (NOT CHAMELEON_SIMULATION)
    add_dependencies(chameleon coreblas_include)
endif()

set_property(TARGET chameleon PROPERTY LINKER_LANGUAGE Fortran)
set_property(TARGET chameleon PROPERTY Fortran_MODULE_DIRECTORY "${CMAKE_BINARY_DIR}/include")
set_property(TARGET chameleon PROPERTY INSTALL_NAME_DIR "${CMAKE_INSTALL_PREFIX}/lib")

# installation
# ------------
install(TARGETS chameleon
        DESTINATION lib)

###
### END CMakeLists.txt
###
