/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2017 Inria. All rights reserved.
 * @copyright (c) 2012-2017 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 * @file zgetrf_sp.c
 *
 *  MORSE computational routines
 *  MORSE is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver
 *
 * @version 2.6.0
 * @author Omar Zenati
 * @author Terry Cojean
 * @date 2011-07-06
 *
 * @precisions normal z -> s d c
 *
 **/
#include "control/common.h"

/***************************************************************************//**
 *
 * @ingroup MORSE_Complex64_t
 *
 *  MORSE_zgetrf_sp - Computes the LU decomposition of a matrix A.
 *  The factorization has the form
 *
 *    \f[ A = \{_{L\times U} \f]
 *
 *  where U is an upper triangular matrix and L is a lower triangular matrix.
 *
 *******************************************************************************
 *
 * @param[in] M
 *          The order of the matrix A. M >= 0.
 *
 * @param[in] N
 *          The order of the matrix A. N >= 0.
 *
 * @param[in,out] A
 *          On entry, the matrix A.
 *          On exit, if return value = 0, the factor U or L from the LU decomposition
 *          A = LU.
 *
 * @param[in] LDA
 *          The leading dimension of the array A. LDA >= max(1,N).
 *
 *******************************************************************************
 *
 * @return
 *          \retval MORSE_SUCCESS successful exit
 *          \retval <0 if -i, the i-th argument had an illegal value
 *          \retval >0 if i, the leading minor of order i of A is not positive definite, so the
 *               factorization could not be completed, and the solution has not been computed.
 *
 *******************************************************************************
 *
 * @sa MORSE_zgetrf_sp_Tile
 * @sa MORSE_zgetrf_sp_Tile_Async
 * @sa MORSE_cgetrf_sp
 * @sa MORSE_dgetrf_sp
 * @sa MORSE_sgetrf_sp
 *
 ******************************************************************************/
int MORSE_zgetrf_sp(int M, int N, MORSE_Complex64_t *A, int LDA,
                    double criteria, int *nb_pivot)
{
    int NB;
    int status;
    MORSE_context_t *morse;
    MORSE_sequence_t *sequence = NULL;
    MORSE_request_t request = MORSE_REQUEST_INITIALIZER;
    MORSE_desc_t descA;

    morse = morse_context_self();
    if (morse == NULL) {
        morse_fatal_error("MORSE_zgetrf_sp", "MORSE not initialized");
        return MORSE_ERR_NOT_INITIALIZED;
    }
    /* Check input arguments */
    if (M < 0) {
        morse_error("MORSE_zgetrf_sp", "illegal value of M");
        return -2;
    }
    if (N < 0) {
        morse_error("MORSE_zgetrf_sp", "illegal value of N");
        return -2;
    }
    if (LDA < chameleon_max(1, M)) {
        morse_error("MORSE_zgetrf_sp", "illegal value of LDA");
        return -4;
    }
    /* Quick return */
    if ((chameleon_max(M, 0) == 0) || (chameleon_max(N, 0) == 0))
        return MORSE_SUCCESS;

    /* Set NT */
    NB = MORSE_NB;

    /* Registering nb_pivot */
    int nb = 0;
    MORSE_data_handle_t nb_pivot_handle;
    RUNTIME_vector_data_register(&nb_pivot_handle, 0, (uintptr_t)&nb, 1, sizeof(int));

    morse_sequence_create(morse, &sequence);

    /* if ( MORSE_TRANSLATION == MORSE_OUTOFPLACE ) { */
    morse_zooplap2tile( descA, A, NB, NB, LDA, N, 0, 0, M, N,
                        sequence, &request,
                        morse_desc_mat_free(&(descA)));
    /* } else { */
    /*     magma_ziplap2tile(  descA, A, NB, NB, LDA, N, 0, 0, N, N); */
    /* } */

    /* Call the tile interface */
    MORSE_zgetrf_sp_Tile_Async(&descA, sequence, &request, criteria, &nb_pivot_handle);

    /* if ( MORSE_TRANSLATION == MORSE_OUTOFPLACE ) { */
    morse_zooptile2lap( descA, A, NB, NB, LDA, N , sequence, &request );
    morse_sequence_wait(morse, sequence);
    morse_desc_mat_free(&descA);
    /* } else { */
    /*     magma_ziptile2lap( descA, A, NB, NB, LDA, N ); */
    /*     morse_barrier( magma ); */
    /* } */

    status = sequence->status;
    morse_sequence_destroy(morse, sequence);

    RUNTIME_data_unregister(&nb_pivot_handle);

    RUNTIME_data_acquire(&nb_pivot_handle);
    RUNTIME_data_release(&nb_pivot_handle);
    *nb_pivot = nb;

    return status;
}

/***************************************************************************//**
 *
 * @ingroup MORSE_Complex64_t_Tile
 *
 *  MORSE_zgetrf_sp_Tile - Computes the LU decomposition of matrix.
 *  Tile equivalent of MORSE_zgetrf_sp().
 *  Operates on matrices stored by tiles.
 *  All matrices are passed through descriptors.
 *  All dimensions are taken from the descriptors.
 *
 *******************************************************************************
 *
 *
 * @param[in] A
 *          On entry, the matrix A.
 *          On exit, if return value = 0, the factor U or L from the LU decomposition
 *          A = LU.
 *
 *******************************************************************************
 *
 * @return
 *          \retval MORSE_SUCCESS successful exit
 *          \retval >0 if i, the leading minor of order i of A is not positive definite, so the
 *               factorization could not be completed, and the solution has not been computed.
 *
 *******************************************************************************
 *
 * @sa MORSE_zgetrf_sp
 * @sa MORSE_zgetrf_sp_Tile_Async
 * @sa MORSE_cgetrf_sp_Tile
 * @sa MORSE_dgetrf_sp_Tile
 * @sa MORSE_sgetrf_sp_Tile
 *
 ******************************************************************************/
int MORSE_zgetrf_sp_Tile(MORSE_desc_t *A, double criteria)
{
    MORSE_context_t *morse;
    MORSE_sequence_t *sequence = NULL;
    MORSE_request_t request = MORSE_REQUEST_INITIALIZER;
    int status;

    morse = morse_context_self();
    if (morse== NULL) {
        morse_fatal_error("MORSE_zgetrf_sp_Tile", "MORSE not initialized");
        return MORSE_ERR_NOT_INITIALIZED;
    }

    int nb = 0;
    MORSE_data_handle_t nb_pivot_handle;
    RUNTIME_data_alloc(&nb_pivot_handle);
    RUNTIME_vector_data_register(&nb_pivot_handle, 0, (uintptr_t)&nb, 1, sizeof(int));

    morse_sequence_create(morse, &sequence);
    MORSE_zgetrf_sp_Tile_Async(A, sequence, &request, criteria, &nb_pivot_handle);
    morse_sequence_wait(morse, sequence);
    RUNTIME_desc_getoncpu(A);

    status = sequence->status;
    morse_sequence_destroy(morse, sequence);

    RUNTIME_data_unregister(&nb_pivot_handle);
    RUNTIME_data_free(&nb_pivot_handle);

    return status;
}

/***************************************************************************//**
 *
 * @ingroup MORSE_Complex64_t_Tile_Async
 *
 *  MORSE_zgetrf_sp_Tile_Async - Computes the LU decomposition
 *  Non-blocking equivalent of MORSE_zgetrf_sp_Tile().
 *  May return before the computation is finished.
 *  Allows for pipelining of operations ar runtime.
 *
 *******************************************************************************
 *
 * @param[in] sequence
 *          Identifies the sequence of function calls that this call belongs to
 *          (for completion checks and exception handling purposes).
 *
 * @param[out] request
 *          Identifies this function call (for exception handling purposes).
 *
 *******************************************************************************
 *
 * @sa MORSE_zgetrf_sp
 * @sa MORSE_zgetrf_sp_Tile
 * @sa MORSE_cgetrf_sp_Tile_Async
 * @sa MORSE_dgetrf_sp_Tile_Async
 * @sa MORSE_sgetrf_sp_Tile_Async
 * @sa MORSE_zpotrs_Tile_Async
 *
 ******************************************************************************/
int MORSE_zgetrf_sp_Tile_Async(MORSE_desc_t *A,
			       MORSE_sequence_t *sequence, MORSE_request_t *request,
			       double criteria, MORSE_data_handle_t *nb_pivot_handle)
{
    MORSE_context_t *morse;

    morse = morse_context_self();
    if (morse == NULL) {
        morse_fatal_error("MORSE_zgetrf_sp_Tile", "MORSE not initialized");
        return MORSE_ERR_NOT_INITIALIZED;
    }
    if (sequence == NULL) {
        morse_fatal_error("MORSE_zgetrf_sp_Tile", "NULL sequence");
        return MORSE_ERR_UNALLOCATED;
    }
    if (request == NULL) {
        morse_fatal_error("MORSE_zgetrf_sp_Tile", "NULL request");
        return MORSE_ERR_UNALLOCATED;
    }
    /* Check sequence status */
    if (sequence->status == MORSE_SUCCESS)
        request->status = MORSE_SUCCESS;
    else
        return morse_request_fail(sequence, request, MORSE_ERR_SEQUENCE_FLUSHED);

    /* Check descriptors for correctness */
    if (morse_desc_check(A) != MORSE_SUCCESS) {
        morse_error("MORSE_zgetrf_nopiv_Tile", "invalid first descriptor");
        return morse_request_fail(sequence, request, MORSE_ERR_ILLEGAL_VALUE);
    }

    if (A->nb != A->mb) {
        morse_error("MORSE_zgetrf_sp_Tile", "only square tiles supported");
        return morse_request_fail(sequence, request, MORSE_ERR_ILLEGAL_VALUE);
    }

    morse_pzgetrf_sp(A, criteria, nb_pivot_handle, sequence, request);

    return MORSE_SUCCESS;
}
