/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2017 Inria. All rights reserved.
 * @copyright (c) 2012-2017 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 * @precisions normal z -> c d s
 *
 **/
#define _TYPE  MORSE_Complex64_t
#define _PREC  double
#define _LAMCH LAPACKE_dlamch_work

#define _NAME  "MORSE_zgetrf_pp_Tile"
/* See Lawn 41 page 120 */
#define _FMULS FMULS_GETRF(M, N)
#define _FADDS FADDS_GETRF(M, N)

#include "./timing.c"

static int
RunTest(int *iparam, double *dparam, morse_time_t *t_)
{
    PASTE_CODE_IPARAM_LOCALS( iparam );

    if ( M != N && check ) {
        fprintf(stderr, "Check cannot be perfomed with M != N\n");
        check = 0;
    }

    /* Allocate Data */
    PASTE_CODE_ALLOCATE_MATRIX_TILE( descA, 1, MORSE_Complex64_t, MorseComplexDouble, LDA, M, N );
    PASTE_CODE_ALLOCATE_MATRIX( piv, 1, int, chameleon_max(M, N), 1 );

    MORSE_zplrnt_Tile(descA, 3456);

    /* Save A in lapack layout for check */
    PASTE_TILE_TO_LAPACK( descA, A2, check, MORSE_Complex64_t, LDA, N );
    PASTE_CODE_ALLOCATE_MATRIX( ipiv2, check, int, chameleon_max(M, N), 1 );

    /* Save AT in lapack layout for check */
    if ( check ) {
        LAPACKE_zgetrf_work(LAPACK_COL_MAJOR, M, N, A2, LDA, ipiv2 );
    }

    START_TIMING();
    MORSE_zgetrf_pp_Tile( descA, piv);
    STOP_TIMING();

    if ( check )
    {
        int64_t i;
        double *work = (double *)malloc(chameleon_max(M, N)*sizeof(double));
        PASTE_TILE_TO_LAPACK( descA, A, 1, MORSE_Complex64_t, LDA, chameleon_min(M,N) );

        /* Check ipiv */
        for(i=0; i<chameleon_min(M,N); i++)
        {
            if( piv[i] != ipiv2[i] ) {
                fprintf(stdout, "\nCHAMELEON (piv[%ld] = %d, A[%ld] = %e) / LAPACK (piv[%ld] = %d, A[%ld] = [%e])\n",
                        i, piv[i],  i, creal(A[  i * LDA + i ]),
                        i, ipiv2[i], i, creal(A2[ i * LDA + i ]));
                break;
            }
        }

        dparam[IPARAM_ANORM] = LAPACKE_zlange_work(LAPACK_COL_MAJOR,
                                                   morse_lapack_const(MorseMaxNorm),
                                                   M, N, A, LDA, work);
        dparam[IPARAM_XNORM] = LAPACKE_zlange_work(LAPACK_COL_MAJOR,
                                                   morse_lapack_const(MorseMaxNorm),
                                                   M, N, A2, LDA, work);
        dparam[IPARAM_BNORM] = 0.0;

        CORE_zgeadd( MorseNoTrans, M, N, -1.0, A, LDA, 1.0, A2, LDA);

        dparam[IPARAM_RES] = LAPACKE_zlange_work(LAPACK_COL_MAJOR,
                                                 morse_lapack_const(MorseMaxNorm),
                                                 M, N, A2, LDA, work);

        free( A );
        free( A2 );
        free( ipiv2 );
        free( work );
    }

    PASTE_CODE_FREE_MATRIX(descA);
    free(piv);
    return 0;
}
