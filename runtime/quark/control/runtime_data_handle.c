/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2016 Inria. All rights reserved.
 * @copyright (c) 2012-2014, 2016 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 * @file runtime_data_handle.c
 *
 *  MORSE auxiliary routines
 *  MORSE is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver
 *
 * @author Terry Cojean
 * @date 2017-02-11
 *
 **/
#include "runtime/quark/include/morse_quark.h"

void RUNTIME_data_alloc(MORSE_data_handle_t* data_handle)
{
}

void RUNTIME_data_free(MORSE_data_handle_t* data_handle)
{
}

void RUNTIME_vector_data_register(MORSE_data_handle_t* data_handle, uint32_t home_node,
                                  uintptr_t ptr, uint32_t nx, size_t elemsize)
{
}

void RUNTIME_matrix_data_register(MORSE_data_handle_t* data_handle, uint32_t home_node,
                                  uintptr_t ptr, uint32_t ld, uint32_t nx,
                                  uint32_t ny, size_t elemsize)
{
}

void RUNTIME_data_acquire(MORSE_data_handle_t *data_handle)
{
}

void RUNTIME_data_release(MORSE_data_handle_t *data_handle)
{
}

void RUNTIME_data_unregister(MORSE_data_handle_t* data_handle)
{
}

void RUNTIME_data_acquire_all(MORSE_desc_t *desc, int mode)
{
}
