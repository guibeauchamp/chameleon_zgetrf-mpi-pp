/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2017 Inria. All rights reserved.
 * @copyright (c) 2012-2017 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 * @file runtime_options.c
 *
 *  MORSE auxiliary routines
 *  MORSE is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver,
 *  and INRIA Bordeaux Sud-Ouest
 *
 * @version 0.9.0
 * @author Terry Cojean
 * @date 2017-02-13
 *
 **/
#include "runtime/parsec/include/morse_parsec.h"

void RUNTIME_unalloc_threads(MORSE_option_t *options)
{
    return;
}

void RUNTIME_alloc_threads(MORSE_option_t *options, int ncores, int *cores)
{
    return;
}
