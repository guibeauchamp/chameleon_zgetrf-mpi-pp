/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2017 Inria. All rights reserved.
 * @copyright (c) 2012-2017 IPB. All rights reserved.
 *
 **/

/**
 *
 * @file codelet_zlaswp.c
 *
 *  MORSE codelets kernel
 *  MORSE is a software package provided by Inria Bordeaux - Sud-Ouest, LaBRI,
 *  University of Bordeaux, Bordeaux INP
 *
 * @version 2.6.0
 * @author Mathieu Faverge
 * @author Terry Cojean
 * @date 2010-11-15
 * @precisions normal z -> c d s
 *
 **/
#include "runtime/starpu/include/morse_starpu.h"
#include "runtime/starpu/include/runtime_codelet_z.h"

void MORSE_TASK_zswpp(MORSE_option_t *options,
                      MORSE_desc_t *A, MORSE_desc_t *Aij, int Aijm, int Aijn,
                      MORSE_desc_t *B, int Bm, int Bn, int i1,  int i2,
                      MORSE_desc_t *ipiv, int inc)
{
    if ( morse_desc_islocal( Aij, Aijm, Aijn ) ||  morse_desc_islocal( B, Bm, Bn ) || morse_desc_islocal( ipiv, Aijm, 0 )) {
        struct starpu_codelet *codelet = &cl_zswpp;
        void (*callback)(void*) = options->profiling ? cl_zswpp_callback : NULL;
        int execution_rank = B->get_rankof( B, Bm, Bn );
        starpu_insert_task(starpu_mpi_codelet(codelet),
                           STARPU_VALUE,      &A,                   sizeof(MORSE_desc_t*),
                           STARPU_VALUE,      &i1,                  sizeof(int),
                           STARPU_VALUE,      &i2,                  sizeof(int),
                           STARPU_VALUE,      &inc,                 sizeof(int),
                           STARPU_VALUE,      &Aijm,                sizeof(int),
                           STARPU_VALUE,      &Aijn,                sizeof(int),
                           STARPU_VALUE,      &Bm,                  sizeof(int),
                           STARPU_VALUE,      &Bn,                  sizeof(int),
                           STARPU_RW,         RTBLKADDR(Aij, MORSE_Complex64_t, Aijm, Aijn),
                           STARPU_R,          RTBLKADDR(ipiv, int,Aijm,0),
                           STARPU_RW,         RTBLKADDR(B, MORSE_Complex64_t, Bm, Bn),
                           STARPU_PRIORITY,   options->priority,
#if defined(CHAMELEON_USE_MPI)
                           STARPU_EXECUTE_ON_NODE, execution_rank,
#endif
#if defined(CHAMELEON_CODELETS_HAVE_NAME)
                           STARPU_NAME, "zswpp",
#endif
                           STARPU_CALLBACK,   callback, NULL,
                           0);
    }

}

static void cl_zswpp_cpu_func(void *descr[], void *cl_arg)
{
    int i1, i2, inc;
    int *ipiv;
    int Am, An, Bm, Bn;
    MORSE_Complex64_t *A, *B;
    MORSE_desc_t *descA;

    starpu_codelet_unpack_args(cl_arg, &descA, &i1, &i2, &inc, &Am, &An, &Bm, &Bn);

    A = (MORSE_Complex64_t *)STARPU_MATRIX_GET_PTR(descr[0]);
    ipiv = (int *)STARPU_VECTOR_GET_PTR(descr[1]);
    B = (MORSE_Complex64_t *)STARPU_MATRIX_GET_PTR(descr[2]);

    //Put Bm relative to the submatrix
    Bm -= descA->i/descA->mb;
    CORE_zswpp(descA, Bm, i1, i2, ipiv, inc);
}

void MORSE_TASK_zswptr_ontile(const MORSE_option_t *options,
                              const MORSE_desc_t *descA,
                              const MORSE_desc_t *Aij, int Aijm, int Aijn,
                              int i1,  int i2,
                              MORSE_data_handle_t *ipiv, int inc,
                              const MORSE_desc_t *Akk, int Akkm, int Akkn, int ldak)
{
    int i;
    struct starpu_codelet *codelet = &cl_zswptr_ontile;
    void (*callback)(void*) = options->profiling ? cl_zswptr_ontile_callback : NULL;

    if ( morse_desc_islocal( descA, 0, 0 )) {
        struct starpu_data_descr data[descA->mt+3];

        data[0].handle = RTBLKADDR(Aij, MORSE_Complex64_t, Aijm, Aijn);
        data[0].mode   = STARPU_RW;
        data[1].handle = *(starpu_data_handle_t*)(ipiv->data_handle);
        data[1].mode   = STARPU_R;
        data[2].handle = RTBLKADDR(Akk, MORSE_Complex64_t, Akkm, Akkn);
        data[2].mode   = STARPU_RW;
        for (i=0 ; i< descA->mt ; i++) {
            data[i+3].handle = RTBLKADDR(descA, MORSE_Complex64_t, i, 0);
            data[i+3].mode   = STARPU_RW;
        }

        starpu_insert_task(codelet,
                           STARPU_VALUE, &descA,            sizeof(MORSE_desc_t*),
                           STARPU_VALUE,    &i1,                      sizeof(int),
                           STARPU_VALUE,    &i2,                      sizeof(int),
                           STARPU_VALUE,   &inc,                      sizeof(int),
                           STARPU_VALUE,  &ldak,                      sizeof(int),
                           STARPU_VALUE,  &Aijm,                      sizeof(int),
                           STARPU_VALUE,  &Aijn,                      sizeof(int),
                           STARPU_DATA_MODE_ARRAY, &data, descA->mt + 3,
                           STARPU_PRIORITY, options->priority,
                           STARPU_CALLBACK, callback, NULL,
#if defined(CHAMELEON_CODELETS_HAVE_NAME)
                           STARPU_NAME, "zswptr_ontile",
#endif
                           0);
    }
}


static void cl_zswptr_ontile_cpu_func(void *descr[], void *cl_arg)
{
    int i1, i2, inc, ldak;
    int *ipiv;
    int Am, An;
    MORSE_Complex64_t *A, *Akk;
    MORSE_desc_t *descA;

    A = (MORSE_Complex64_t *)STARPU_MATRIX_GET_PTR(descr[0]);
    ipiv = (int *)STARPU_VECTOR_GET_PTR(descr[1]);
    Akk = (MORSE_Complex64_t *)STARPU_MATRIX_GET_PTR(descr[2]);
    starpu_codelet_unpack_args(cl_arg, &descA, &i1, &i2, &inc, &ldak, &Am, &An);

    CORE_zswptr_ontile(descA, i1, i2, ipiv, inc, Akk, ldak);
}

CODELETS_CPU(zswptr_ontile, STARPU_VARIABLE_NBUFFERS, cl_zswptr_ontile_cpu_func)

CODELETS_CPU(zswpp, 3, cl_zswpp_cpu_func)
