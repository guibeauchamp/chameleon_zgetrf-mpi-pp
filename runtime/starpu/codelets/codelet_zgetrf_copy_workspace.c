/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2017 Inria. All rights reserved.
 * @copyright (c) 2012-2017, 2016 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 *  @file codelet_zgetrf_copy_workspace.c
 *
 *  MORSE codelets kernel
 *  MORSE is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver,
 *  and INRIA Bordeaux Sud-Ouest
 *
 *  @version 2.3.1
 *  @author Omar Zenati
 *  @author Terry Cojean
 *  @date 2011-06-01
 *  @precisions normal z -> c d s
 *
 **/

#include "runtime/starpu/include/morse_starpu.h"
#include "runtime/starpu/include/runtime_codelet_z.h"

#define get_pivot(_a_) (*((int *) (_a_)))
#define get_diagonal(_a_) (_a_ + 1)
#define get_maxrow(_a_) (_a_ + NB + 1)

void MORSE_TASK_zgetrf_copy_workspace(MORSE_option_t *options,
                                      int n, int h, /* int r, int p, */ int nb,
                                      MORSE_desc_t * workspaces, int top_id, int bottom_id)
{
    struct starpu_codelet *codelet = &cl_zgetrf_copy_workspace;
    void (*callback)(void*) = options->profiling ? cl_zgetrf_copy_workspace_callback : NULL;
    int execution_rank = workspaces->get_rankof(workspaces, top_id, 0);

   
  if (morse_desc_islocal(workspaces, top_id, 0)||morse_desc_islocal(workspaces, bottom_id, 0))
  {
    starpu_insert_task(starpu_mpi_codelet(codelet),
		       STARPU_VALUE,    &n,        sizeof(int),
                       STARPU_VALUE,    &h,        sizeof(int),
                       /* STARPU_VALUE,    &r,        sizeof(int), */
		       /* STARPU_VALUE,    &p,        sizeof(int), */
                       STARPU_VALUE,    &nb,       sizeof(int),
                       STARPU_RW,       RTBLKADDR(workspaces,MORSE_Complex64_t,top_id,0),
                       STARPU_R,        RTBLKADDR(workspaces,MORSE_Complex64_t,bottom_id,0),
                       STARPU_PRIORITY, options->priority,
                       STARPU_CALLBACK, callback, NULL,  
#if defined(CHAMELEON_USE_MPI)
                       STARPU_EXECUTE_ON_NODE, execution_rank,
#endif
#if defined(CHAMELEON_CODELETS_HAVE_NAME)
                       STARPU_NAME, "zgetrf_copy_workspace",
#endif
                       0);
  }

}

static void cl_zgetrf_copy_workspace_cpu_func(void *descr[], void *cl_arg)
{
  int N,H,/* R,P, */NB;
  MORSE_Complex64_t *top, *bottom;

  top    = (MORSE_Complex64_t *) STARPU_VECTOR_GET_PTR(descr[0]);
  bottom = (MORSE_Complex64_t *) STARPU_VECTOR_GET_PTR(descr[1]);

  starpu_codelet_unpack_args(cl_arg, &N, &H, /* &R, &P, */ &NB);

  MORSE_Complex64_t *mT = get_maxrow( top );
  MORSE_Complex64_t *mB = get_maxrow( bottom );
  MORSE_Complex64_t *dT = get_diagonal( top );
  MORSE_Complex64_t *dB = get_diagonal( bottom );


  get_pivot( top ) = get_pivot( bottom );
  cblas_zcopy(N, mB, 1,
	         mT, 1);
  cblas_zcopy(N, dB, 1,
	         dT, 1);
}

/*
 * Codelet definition
 */
CODELETS_CPU(zgetrf_copy_workspace, 2, cl_zgetrf_copy_workspace_cpu_func)

