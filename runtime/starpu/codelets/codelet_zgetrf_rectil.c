/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2017 Inria. All rights reserved.
 * @copyright (c) 2012-2017, 2016 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 * @file codelet_zgetrf_nopiv.c
 *
 *  MORSE codelets kernel
 *  MORSE is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver
 *
 * @version 2.6.0
 * @author Omar Zenati
 * @author Terry Cojean
 * @author Mathieu Faverge
 * @author Emmanuel Agullo
 * @author Cedric Castagnede
 * @date 2013-02-01
 * @precisions normal z -> c d s
 *
 **/
#include "runtime/starpu/include/morse_starpu.h"
#include "runtime/starpu/include/runtime_codelet_z.h"

/**
 *
 * @ingroup CORE_MORSE_Complex64_t
 *
 *  CORE_zgetrf_rectil computes an LU factorization of a general diagonal
 *  dominant M-by-N matrix A with pivoting right-looking.
 *
 *  The factorization has the form
 *     A = L * U
 *  where L is lower triangular with unit
 *  diagonal elements (lower trapezoidal if m > n), and U is upper
 *  triangular (upper trapezoidal if m < n).
 *
 *  This is the right-looking Level 3 BLAS version of the algorithm.
 *  WARNING: Your matrix need to be diagonal dominant if you want to call this
 *  routine safely.
 *
 *******************************************************************************
 *
 *  @param[in,out] A
 *          On entry, the M-by-N matrix to be factored.
 *          On exit, the factors L and U from the factorization
 *          A = P*L*U; the unit diagonal elements of L are not stored.
 *
 * @param[out] IPIV
 *         The pivot indices; for 1 <= i <= min(M,N), row i of the
 *         tile was interchanged with row IPIV(i).
 *******************************************************************************
 *
 * @return
 *         \retval MORSE_SUCCESS successful exit
 *         \retval <0 if INFO = -k, the k-th argument had an illegal value
 *         \retval >0 if INFO = k, U(k,k) is exactly zero. The factorization
 *              has been completed, but the factor U is exactly
 *              singular, and division by zero will occur if it is used
 *              to solve a system of equations.
 *
 ******************************************************************************/

void MORSE_TASK_zgetrf_rectil(const MORSE_option_t *options,
                              const MORSE_desc_t *A, MORSE_data_handle_t *IPIV_h)
{
    int i;
    struct starpu_codelet *codelet = &cl_zgetrf_rectil;
    void (*callback)(void*) = options->profiling ? cl_zgetrf_rectil_callback : NULL;
    struct starpu_data_descr data[A->mt+1];

    data[0].handle = *(starpu_data_handle_t*)(IPIV_h->data_handle);
    data[0].mode   = STARPU_RW;
    for (i=0 ; i< A->mt ; i++) {
        data[i+1].handle = RTBLKADDR(A, MORSE_Complex64_t, i, 0);
        data[i+1].mode   = STARPU_RW;
    }

    starpu_insert_task(starpu_mpi_codelet(codelet),
                       STARPU_VALUE,     &A,            sizeof(MORSE_desc_t*),
                       STARPU_DATA_MODE_ARRAY, &data, A->mt+1,
                       STARPU_PRIORITY,  options->priority,
                       STARPU_SCHED_CTX, options->panel_threads,
                       STARPU_CALLBACK,  callback,
                       STARPU_POSSIBLY_PARALLEL, 1,
                       0);
}

/*
 * Codelet CPU
 */
static void cl_zgetrf_rectil_cpu_func(void *descr[], void *cl_arg)
{
    MORSE_desc_t *A = NULL;
    int *IPIV = (int *)STARPU_VECTOR_GET_PTR(descr[0]);
    unsigned sched_ctx = starpu_task_get_current()->sched_ctx;
    int info[3];

    starpu_codelet_unpack_args(cl_arg, &A);

    int rank = starpu_sched_ctx_get_worker_rank(sched_ctx);
    info[1] = rank == -1 ? 0 : rank;
    info[2] = rank == -1 ? 1 : starpu_sched_ctx_get_nworkers(sched_ctx);

    CORE_zgetrf_rectil(A, IPIV, info );
    if (*info!=0) {
        fprintf(stderr, "Core_zgetrf_rectil failed at column/line %d.\n", *info);
        exit(-1);
    }
}

/*
 * Codelet definition
 */
CODELETS_CPU(zgetrf_rectil, STARPU_VARIABLE_NBUFFERS, cl_zgetrf_rectil_cpu_func)
