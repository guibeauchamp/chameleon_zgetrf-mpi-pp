/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2017 Inria. All rights reserved.
 * @copyright (c) 2012-2017 IPB. All rights reserved.
 *
 **/

/**
 *
 * @file codelet_zlaswp.c
 *
 *  MORSE codelets kernel
 *  MORSE is a software package provided by Inria Bordeaux - Sud-Ouest, LaBRI,
 *  University of Bordeaux, Bordeaux INP
 *
 * @version 2.6.0
 * @author Mathieu Faverge
 * @author Terry Cojean
 * @date 2010-11-15
 * @precisions normal z -> c d s
 *
 **/
#include "runtime/starpu/include/morse_starpu.h"
#include "runtime/starpu/include/runtime_codelet_z.h"

void MORSE_TASK_zlaswp(const MORSE_option_t *options,
                       int n, const MORSE_desc_t *A, int Am, int An, int lda,
                       int i1,  int i2,
                       MORSE_data_handle_t *ipiv, int inc)
{
    struct starpu_codelet *codelet = &cl_zlaswp;
    void (*callback)(void*) = options->profiling ? cl_zlaswp_callback : NULL;
    starpu_insert_task(codelet,
                       STARPU_VALUE,    &n,                      sizeof(int),
                       STARPU_RW,        RTBLKADDR(A, MORSE_Complex64_t, Am, An),
                       STARPU_VALUE,  &lda,                      sizeof(int),
                       STARPU_VALUE,   &i1,                      sizeof(int),
                       STARPU_VALUE,   &i2,                      sizeof(int),
                       STARPU_R,      *(starpu_data_handle_t*)(ipiv->data_handle),
                       STARPU_VALUE,  &inc,                      sizeof(int),
                       STARPU_PRIORITY,    options->priority,
                       STARPU_CALLBACK,    callback, NULL,
#if defined(CHAMELEON_CODELETS_HAVE_NAME)
                       STARPU_NAME, "zlaswp",
#endif
                       0);
}

static void cl_zlaswp_cpu_func(void *descr[], void *cl_arg)
{
    int n, lda, i1, i2, inc;
    int *ipiv;
    MORSE_Complex64_t *A;

    A = (MORSE_Complex64_t *)STARPU_MATRIX_GET_PTR(descr[0]);
    ipiv = (int *)STARPU_VECTOR_GET_PTR(descr[1]);
    starpu_codelet_unpack_args(cl_arg, &n, &lda, &i1, &i2, &inc);
    CORE_zlaswp(n, A, lda, i1, i2, ipiv, inc);
}

/* descA is a submatrix representing a panel (with multiple subtiles) */
void MORSE_TASK_zlaswp_ontile(const MORSE_option_t *options,
                              const MORSE_desc_t *descA,
                              const MORSE_desc_t *Aij, int Aijm, int Aijn,
                              int i1,  int i2,
                              MORSE_data_handle_t *ipiv, int inc)
{
    int i;
    struct starpu_codelet *codelet = &cl_zlaswp_ontile;
    void (*callback)(void*) = options->profiling ? cl_zlaswp_ontile_callback : NULL;
    struct starpu_data_descr data[descA->mt+1];

    data[0].handle = *(starpu_data_handle_t*)(ipiv->data_handle);
    data[0].mode   = STARPU_R;
    for (i=0 ; i< descA->mt ; i++) {
        data[i+1].handle = RTBLKADDR(descA, MORSE_Complex64_t, i, 0);
        data[i+1].mode   = STARPU_RW;
    }

    starpu_insert_task(codelet,
                       STARPU_VALUE,     &descA,            sizeof(MORSE_desc_t*),
                       STARPU_VALUE,        &i1,                      sizeof(int),
                       STARPU_VALUE,        &i2,                      sizeof(int),
                       STARPU_VALUE,       &inc,                      sizeof(int),
                       STARPU_VALUE,      &Aijm,                      sizeof(int),
                       STARPU_VALUE,      &Aijn,                      sizeof(int),
                       STARPU_DATA_MODE_ARRAY, &data, descA->mt+1,
                       STARPU_PRIORITY,    options->priority,
                       STARPU_CALLBACK,    callback,
#if defined(CHAMELEON_CODELETS_HAVE_NAME)
                       STARPU_NAME, "zlaswp_ontile",
#endif
                       0);
}


static void cl_zlaswp_ontile_cpu_func(void *descr[], void *cl_arg)
{
    int i1, i2, inc;
    int *ipiv;
    int Am, An;
    MORSE_Complex64_t *A;
    MORSE_desc_t *descA;

    ipiv = (int *)STARPU_VECTOR_GET_PTR(descr[0]);
    A = (MORSE_Complex64_t *)STARPU_MATRIX_GET_PTR(descr[1]);
    starpu_codelet_unpack_args(cl_arg, &descA, &i1, &i2, &inc, &Am, &An);

    CORE_zlaswp_ontile(descA, i1, i2, ipiv, inc);
}

/* descA is a submatrix representing a panel (with multiple subtiles) */
void MORSE_TASK_zlaswpc_ontile(const MORSE_option_t *options,
                               const MORSE_desc_t *descA,
                               const MORSE_desc_t *Aij, int Aijm, int Aijn,
                               int i1,  int i2,
                               MORSE_data_handle_t *ipiv, int inc)
{
    int i;
    struct starpu_codelet *codelet = &cl_zlaswpc_ontile;
    void (*callback)(void*) = options->profiling ? cl_zlaswpc_ontile_callback : NULL;
    struct starpu_data_descr data[descA->mt+1];

    data[0].handle = *(starpu_data_handle_t*)(ipiv->data_handle);
    data[0].mode   = STARPU_R;
    for (i=0 ; i< descA->mt ; i++) {
        data[i+1].handle = RTBLKADDR(descA, MORSE_Complex64_t, i, 0);
        data[i+1].mode   = STARPU_RW;
    }

    starpu_insert_task(
        codelet,
        STARPU_VALUE,     &descA,              sizeof(MORSE_desc_t),
        STARPU_VALUE,        &i1,                      sizeof(int),
        STARPU_VALUE,        &i2,                      sizeof(int),
        STARPU_VALUE,       &inc,                      sizeof(int),
        STARPU_DATA_MODE_ARRAY, &data, descA->mt+1,
        STARPU_PRIORITY,    options->priority,
        STARPU_CALLBACK,    callback,
#if defined(CHAMELEON_CODELETS_HAVE_NAME)
        STARPU_NAME, "zlaswp_ontile",
#endif
        0);
}


static void cl_zlaswpc_ontile_cpu_func(void *descr[], void *cl_arg)
{
    int i1, i2, inc;
    int *ipiv;
    MORSE_Complex64_t *A;
    MORSE_desc_t *descA;

    starpu_codelet_unpack_args(cl_arg, &descA, &i1, &i2, &ipiv, &inc);
    CORE_zlaswpc_ontile(descA, i1, i2, ipiv, inc);
}

/*
 * Codelet definition
 */
CODELETS_CPU(zlaswp, 2, cl_zlaswp_cpu_func)

CODELETS_CPU(zlaswp_ontile, STARPU_VARIABLE_NBUFFERS, cl_zlaswp_ontile_cpu_func)

CODELETS_CPU(zlaswpc_ontile, STARPU_VARIABLE_NBUFFERS, cl_zlaswpc_ontile_cpu_func)
