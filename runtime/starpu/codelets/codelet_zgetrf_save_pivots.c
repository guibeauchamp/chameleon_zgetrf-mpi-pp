/**
 *
 *  @file codelet_zgetrf_save_pivots.c
 *
 *  MAGMA codelets kernel
 *  MAGMA is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver,
 *  and INRIA Bordeaux Sud-Ouest
 *
 *  @version 2.3.1
 *  @author Omar Zenati
 *  @date 2011-06-01
 *  @precisions normal z -> c d s
 *
 **/

#include "runtime/starpu/include/morse_starpu.h"
#include "runtime/starpu/include/runtime_codelet_z.h"

#define get_pivot(_a_) (*((int *) (_a_)))

void MORSE_TASK_zgetrf_save_pivots(MORSE_option_t *options,
                                   int h,
                                   MORSE_desc_t * IPIV,
                                   MORSE_desc_t * workspace,
                                   int k)
{
    struct starpu_codelet *codelet = &cl_zgetrf_save_pivots;
    void (*callback)(void*) = options->profiling ? cl_zgetrf_save_pivots_callback : NULL;
    int execution_rank = IPIV->get_rankof(IPIV, k, 0);

 if (morse_desc_islocal(workspace, k, 0)||morse_desc_islocal(IPIV, k, 0))
  {
    starpu_insert_task(starpu_mpi_codelet(codelet),
                       STARPU_VALUE,    &h,        sizeof(int),
                       STARPU_RW,       RTBLKADDR(IPIV,int,k,0),
                       STARPU_R,        RTBLKADDR(workspace,MORSE_Complex64_t,k,0),
                       STARPU_PRIORITY, options->priority,
                       STARPU_CALLBACK, callback, NULL,  
#if defined(CHAMELEON_USE_MPI)
                       STARPU_EXECUTE_ON_NODE, execution_rank,
#endif
#if defined(CHAMELEON_CODELETS_HAVE_NAME)
                       STARPU_NAME, "zgetrf_save_pivots",
#endif
                       0);
  }

}

static void cl_zgetrf_save_pivots_cpu_func(void *descr[], void *cl_arg)
{
  int H;
  int *pivots;
  MORSE_Complex64_t *mw;

  pivots = (int *)                STARPU_VECTOR_GET_PTR(descr[0]);
  mw     = (MORSE_Complex64_t *) STARPU_VECTOR_GET_PTR(descr[1]);

  starpu_codelet_unpack_args(cl_arg, &H);

  pivots[H] = get_pivot( mw ) + 1;

}

/*
 * Codelet definition
 */
CODELETS_CPU(zgetrf_save_pivots, 2, cl_zgetrf_save_pivots_cpu_func)

