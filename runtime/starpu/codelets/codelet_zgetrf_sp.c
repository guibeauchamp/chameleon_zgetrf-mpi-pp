/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2017 Inria. All rights reserved.
 * @copyright (c) 2012-2017, 2016, 2017, Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 * @file codelet_zgetrf_sp.c
 *
 *  MORSE codelets kernel
 *  MORSE is a software package provided by Inria Bordeaux - Sud-Ouest, LaBRI,
 *  University of Bordeaux, Bordeaux INP
 *
 * @version 1.0.0
 * @author Omar Zenati
 * @author Terry Cojean
 * @date 2011-06-01
 * @precisions normal z -> c d s
 *
 **/

#include "runtime/starpu/include/morse_starpu.h"
#include "runtime/starpu/include/runtime_codelet_z.h"

/**
 *
 * @ingroup CORE_MORSE_Complex64_t
 *
 **/
void MORSE_TASK_zgetrf_sp( MORSE_option_t *options,
                           int m, int n,
                           MORSE_desc_t *A, int Am, int An,
                           double criteria, MORSE_data_handle_t *nb_pivot)
{
    struct starpu_codelet *zgetrf_sp_codelet;
    void (*callback)(void*) = options->profiling ? cl_zgetrf_sp_callback : NULL;
    int lda = BLKLDD( A, Am );

    zgetrf_sp_codelet = &cl_zgetrf_sp;

    if ( morse_desc_islocal( A, Am, An ) ) {
        starpu_insert_task(zgetrf_sp_codelet,
                           STARPU_VALUE,    &m,        sizeof(int),
                           STARPU_VALUE,    &n,        sizeof(int),
                           STARPU_RW,       RTBLKADDR( A, MORSE_Complex64_t, Am, An ),
                           STARPU_VALUE,    &lda,      sizeof(int),
                           STARPU_VALUE,    &criteria, sizeof(int),
                           STARPU_RW,       *(starpu_data_handle_t *)nb_pivot->data_handle,
                           STARPU_PRIORITY, options->priority,
                           STARPU_CALLBACK, callback,
#if defined(CHAMELEON_CODELETS_HAVE_NAME)
                           STARPU_NAME, "zgetrf_sp",
#endif
                           0);
    }
}

#if !defined(CHAMELEON_SIMULATION)
static void cl_zgetrf_sp_cpu_func(void *descr[], void *cl_arg)
{
    int M,N;
    MORSE_Complex64_t *A;
    int LDA;
    double CRITERIA;
    int *nb_pivot;

    A = (MORSE_Complex64_t *)STARPU_MATRIX_GET_PTR(descr[0]);
    nb_pivot = (int *) STARPU_MATRIX_GET_PTR(descr[1]);

    starpu_codelet_unpack_args(cl_arg, &M, &N, &LDA, &CRITERIA);
    CORE_zgetrf_sp( M, N, A, LDA, CRITERIA, nb_pivot );
}

#ifdef CHAMELEON_USE_MAGMA
static void cl_zgetrf_sp_cuda_func(void *descr[], void *cl_arg)
{
    int M;
    int N;
    cuDoubleComplex *A;
    int LDA;
    double CRITERIA;
    int *nb_pivot;
    int info = 0;

    A = (cuDoubleComplex *)STARPU_MATRIX_GET_PTR(descr[0]);
    nb_pivot = (int *) STARPU_MATRIX_GET_PTR(descr[1]);

    starpu_codelet_unpack_args(cl_arg, &M, &N, &LDA, &CRITERIA);

    CUDA_zgetrf_sp(M, N, A, LDA, CRITERIA, nb_pivot, &info);

    cudaThreadSynchronize();
}
#endif
#endif /* !defined(CHAMELEON_SIMULATION) */

/*
 * Codelet definition
 */
#if defined CHAMELEON_USE_MAGMA
CODELETS(zgetrf_sp, 2, cl_zgetrf_sp_cpu_func, cl_zgetrf_sp_cuda_func, 0)
#else
CODELETS_CPU(zgetrf_sp, 2, cl_zgetrf_sp_cpu_func)
#endif
