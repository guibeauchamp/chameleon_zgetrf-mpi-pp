/**
 *
 *  @file codelet_zswap_insert_rows.c
 *
 *  MORSE codelets kernel
 *  MORSE is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver,
 *  and INRIA Bordeaux Sud-Ouest
 *
 *  @version 2.3.1
 *  @author Omar Zenati
 *  @author Terry Cojean
 *  @date 2011-06-01
 *  @precisions normal z -> c d s
 *
 **/

#include "runtime/starpu/include/morse_starpu.h"
#include "runtime/starpu/include/runtime_codelet_z.h"

void MORSE_TASK_zswp_extract_rows(MORSE_option_t *options,
                                  int nb_pivots, int global_MB,
                                  int K, int SRC_M, int DST_M,
                                  int NB, int MB,
                                  MORSE_desc_t *perms,
                                  MORSE_desc_t *A, int Am, int An,
                                  MORSE_data_handle_t *rows_handle)
{
    void (*callback)(void*) = options->profiling ? cl_zswp_extract_rows_callback : NULL;
    struct starpu_codelet *codelet = &cl_zswp_extract_rows;
    int LDA = BLKLDD(A, Am);

    if (nb_pivots > 0) {
            starpu_task_insert(starpu_mpi_codelet(codelet),
                               STARPU_VALUE,    &nb_pivots, sizeof(int),
                               STARPU_VALUE,    &global_MB,    sizeof(int),
                               STARPU_VALUE,    &K,         sizeof(int),
                               STARPU_VALUE,    &SRC_M,     sizeof(int),
                               STARPU_VALUE,    &DST_M,     sizeof(int),
                               STARPU_VALUE,    &NB,        sizeof(int),
                               STARPU_VALUE,    &MB,        sizeof(int),
                               STARPU_VALUE,    &LDA,       sizeof(int),
                               STARPU_R,    RTBLKADDR(perms, int, K, 0),
                               STARPU_R,        RTBLKADDR(A, MORSE_Complex64_t, Am, An),
                               STARPU_W,   *(starpu_data_handle_t *)rows_handle->data_handle,
                               STARPU_PRIORITY, options->priority,
                               STARPU_CALLBACK, callback,
#if defined(CHAMELEON_CODELETS_HAVE_NAME)
                               STARPU_NAME, "zswp_extract_rows",
#endif
                               0);
    }
}

static void cl_zswp_extract_rows_cpu_func(void *descr[], void *cl_arg)
{
    int K, SRC_M, DST_M, NB, MB, LDA;
    int *perm, nb_pivots, global_MB;
    MORSE_Complex64_t *A;
    MORSE_Complex64_t *rows;

    perm        =               (int *) STARPU_VECTOR_GET_PTR(descr[0]);
    A           = (MORSE_Complex64_t *) STARPU_MATRIX_GET_PTR(descr[1]);
    rows        = (MORSE_Complex64_t *) STARPU_VECTOR_GET_PTR(descr[2]);

    starpu_codelet_unpack_args(cl_arg, &nb_pivots, &global_MB, &K, &SRC_M, &DST_M, &NB, &MB, &LDA);

    CORE_zswp_extract_rows(nb_pivots, global_MB, K, SRC_M, DST_M, A, perm, rows, NB, MB, LDA);
 }

#ifdef CHAMELEON_USE_CUDA
static void cl_zswp_extract_rows_cuda_func(void *descr[], void *cl_arg)
{
    int K, SRC_M, DST_M, NB, MB, LDA;
    int *perm, nb_pivots, global_MB;
    cuDoubleComplex *A;
    cuDoubleComplex *rows;

    perm        =               (int *) STARPU_VECTOR_GET_PTR(descr[0]);
    A           = (cuDoubleComplex *) STARPU_MATRIX_GET_PTR(descr[1]);
    rows        = (cuDoubleComplex *) STARPU_VECTOR_GET_PTR(descr[2]);

    starpu_codelet_unpack_args(cl_arg, &nb_pivots, &global_MB, &K, &SRC_M, &DST_M, &NB, &MB, &LDA);

    CUDA_zswp_extract_rows(global_MB, perm, A, rows, K, SRC_M, DST_M, NB, MB, LDA, RUNTIME_cuda_get_local_stream());

    return;
}
#endif

/*
 * Codelet definition
 */
CODELETS(zswp_extract_rows, 3, cl_zswp_extract_rows_cpu_func, cl_zswp_extract_rows_cuda_func, 0)
