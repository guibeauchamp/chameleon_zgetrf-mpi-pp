/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2017 Inria. All rights reserved.
 * @copyright (c) 2012-2017, 2016 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 *  @file codelet_zgetrf_compute_swap.c
 *
 *  MORSE codelets kernel
 *  MORSE is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver,
 *  and INRIA Bordeaux Sud-Ouest
 *
 *  @precisions normal z -> c d s
 *
 **/

#include "runtime/starpu/include/morse_starpu.h"
#include "runtime/starpu/include/runtime_codelet_z.h"
#include "morse_struct.h"

volatile int waiting;
volatile starpu_pthread_cond_t cond;
volatile starpu_pthread_mutex_t mutex;

void MORSE_TASK_zgetrf_compute_swap_sync(const MORSE_option_t * options, int k, 
                                        MORSE_desc_t * A, MORSE_desc_t *IPIV, 
                                        MORSE_desc_t *perm_from, MORSE_desc_t *perm_to,
                                        int* nbswaps_from, int* nbswaps_to, int tag){
  STARPU_PTHREAD_COND_INIT(&cond, NULL);
  STARPU_PTHREAD_MUTEX_INIT(&mutex, NULL);
  waiting=1;
  MORSE_TASK_zgetrf_compute_swap(options, k, A, IPIV, perm_from, perm_to,
                      nbswaps_from, nbswaps_to,tag);
  STARPU_PTHREAD_MUTEX_LOCK(&mutex);
  while(waiting)
    STARPU_PTHREAD_COND_WAIT(&cond,&mutex);

  STARPU_PTHREAD_MUTEX_UNLOCK(&mutex);
  STARPU_PTHREAD_COND_DESTROY(&cond);
  STARPU_PTHREAD_MUTEX_DESTROY(&mutex);
  
}

void MORSE_TASK_zgetrf_compute_swap(const MORSE_option_t * options, int k, 
                                    MORSE_desc_t * A, MORSE_desc_t *IPIV,
                                    MORSE_desc_t *perm_from, MORSE_desc_t *perm_to,
                                    int* nbswaps_from, int* nbswaps_to, int tag){
  struct starpu_codelet *codelet;
  int execution_rank = A->myrank;
  if(options->swap_grain == MORSE_LINE_SWAPS)
    assert(0 && "TODO : MORSE_LINE_SWAPS compute_swap and submit_swap");
    //TODO
  else if(options->swap_grain == MORSE_TILE_SWAPS){
    void (*callback)(void*) = options->profiling ? NULL : NULL; //TODO
    codelet = &cl_zgetrf_compute_swap_bloc;
    starpu_insert_task(starpu_mpi_codelet(codelet),
                       STARPU_VALUE,    &k,        sizeof(int),
                       STARPU_VALUE,    &A->m,        sizeof(int),
                       STARPU_VALUE,    &A->mt,        sizeof(int),
                       STARPU_VALUE,    &A->mb,        sizeof(int),
                       STARPU_R,       RTBLKADDR(IPIV,int,k,0),
                       STARPU_VALUE,    &nbswaps_from,       sizeof(int*),
                       STARPU_PRIORITY, options->priority,
#if defined(CHAMELEON_USE_MPI)
                       STARPU_EXECUTE_ON_NODE, execution_rank,
#endif
                       STARPU_CALLBACK, callback, NULL,  
#if defined(CHAMELEON_CODELETS_HAVE_NAME)
                       STARPU_NAME, "zgetrf_compute_swap",
#endif
                       0);

    if(morse_desc_islocal(IPIV, k, 0)){
      int node_number;
      RUNTIME_comm_size( &node_number);
      for(int i = 0;i<node_number;i++)
          if (i!=execution_rank)
            starpu_insert_task(starpu_mpi_codelet(codelet),
                         STARPU_VALUE,    &k,        sizeof(int),
                         STARPU_VALUE,    &A->m,        sizeof(int),
                         STARPU_VALUE,    &A->mt,        sizeof(int),
                         STARPU_VALUE,    &A->mb,        sizeof(int),
                         STARPU_R,       RTBLKADDR(IPIV,int,k,0),
                         STARPU_VALUE,    &nbswaps_from,       sizeof(int*),
                         STARPU_PRIORITY, options->priority,
#if defined(CHAMELEON_USE_MPI)
                         STARPU_EXECUTE_ON_NODE, i,
#endif
#if defined(CHAMELEON_CODELETS_HAVE_NAME)
                         STARPU_NAME, "zgetrf_compute_swap",
#endif 
                         STARPU_CALLBACK, callback, NULL,  
                         0);
    }

  }
}

static void cl_zgetrf_compute_swap_bloc_cpu_func(void *descr[], void *cl_arg)
{ 
  int k, m, mt, mb; 
  int *IPIV, *nbswaps_from_upper;

  IPIV  = (int *) STARPU_MATRIX_GET_PTR(descr[0]);

  starpu_codelet_unpack_args(cl_arg, &k, &m, &mt, &mb, &nbswaps_from_upper);

  int tempk  = k * mb;
  int tempkm = k == mt-1 ? m-k*mb : mb;
  int i;
  for (i=0 ; i<tempkm ; i++) {
    int index = (*(IPIV+i)-tempk-1)/((int)mb);
    if (*(IPIV+i) != tempk+i+1)
      nbswaps_from_upper[mt*k+index] += 1;
  }

  waiting=0;
  STARPU_PTHREAD_COND_SIGNAL(&cond);
}

/*
 * Codelet definition
 */
// CODELETS_CPU(zgetrf_compute_swap_line, 1, cl_zgetrf_compute_swap_line_cpu_func)

CODELETS_CPU(zgetrf_compute_swap_bloc, 1, cl_zgetrf_compute_swap_bloc_cpu_func)
