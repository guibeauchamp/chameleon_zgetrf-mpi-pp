/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2017 Inria. All rights reserved.
 * @copyright (c) 2012-2017, 2016 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 *  @file codelet_zgetrf_extract_max.c
 *
 *  MORSE codelets kernel
 *  MORSE is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver,
 *  and INRIA Bordeaux Sud-Ouest
 *
 *  @version 2.3.1
 *  @author Omar Zenati
 *  @author Terry Cojean
 *  @date 2011-06-01
 *  @precisions normal z -> c d s
 *
 **/

#include "runtime/starpu/include/morse_starpu.h"
#include "runtime/starpu/include/runtime_codelet_z.h"

void MORSE_TASK_zgetrf_extract_max(const MORSE_option_t *options,
                                   int k, int m, int n, int h, int nb,
                                   MORSE_desc_t *A, int Am, int An,
                                   MORSE_desc_t *max_workspace)
{
    struct starpu_codelet *codelet = &cl_zgetrf_extract_max;
    void (*callback)(void*) = options->profiling ? cl_zgetrf_extract_max_callback : NULL;
    int lda = BLKLDD( A, Am );
	int execution_rank = A->get_rankof( A, Am, An );

 //   if (morse_desc_islocal(A, Am, An)||morse_desc_islocal(max_workspace, Am, 0))
    {
        starpu_insert_task(starpu_mpi_codelet(codelet),
                           STARPU_VALUE,    &k,        sizeof(int),
                           STARPU_VALUE,    &m,        sizeof(int),
                           STARPU_VALUE,    &n,        sizeof(int),
                           STARPU_VALUE,    &h,        sizeof(int),
                           STARPU_VALUE,    &nb,       sizeof(int),
                           STARPU_VALUE,    &Am,       sizeof(int),
                           STARPU_RW,       RTBLKADDR( A, MORSE_Complex64_t, Am, An ),
                           STARPU_VALUE,    &lda,      sizeof(int),
                           STARPU_RW,    RTBLKADDR(max_workspace,MORSE_Complex64_t,Am,0),
                           STARPU_PRIORITY, options->priority,
#if defined(CHAMELEON_USE_MPI)
                           STARPU_EXECUTE_ON_NODE, execution_rank,
#endif
#if defined(CHAMELEON_CODELETS_HAVE_NAME)
                           STARPU_NAME, "zgetrf_extract_max",
#endif
                           STARPU_CALLBACK, callback, NULL,  
                           0);

    }
}

static void cl_zgetrf_extract_max_cpu_func(void *descr[], void *cl_arg)
{
  int K,M,N,H,NB,AM;
  MORSE_Complex64_t *A;
  int LDA;
  MORSE_Complex64_t *mw;

  A  = (MORSE_Complex64_t *) STARPU_MATRIX_GET_PTR(descr[0]);
  mw = (MORSE_Complex64_t *) STARPU_MATRIX_GET_PTR(descr[1]);

  starpu_codelet_unpack_args(cl_arg, &K, &M, &N, &H, &NB, &AM, &LDA);

  CORE_zgetrf_extract_max(A, mw, K, M, N, H, NB, AM, LDA);

}

/*
 * Codelet definition
 */
CODELETS_CPU(zgetrf_extract_max, 2, cl_zgetrf_extract_max_cpu_func)
