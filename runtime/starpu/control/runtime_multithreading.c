/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2017 Inria. All rights reserved.
 * @copyright (c) 2012-2017 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 * @file runtime_options.c
 *
 *  MORSE auxiliary routines
 *  MORSE is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver,
 *  and INRIA Bordeaux Sud-Ouest
 *
 * @version 0.9.0
 * @author Terry Cojean
 * @date 2017-02-13
 *
 **/
#include "runtime/starpu/include/morse_starpu.h"
#include <starpu_sched_ctx.h>
#ifdef STARPU_HAVE_HWLOC
#include <hwloc.h>

void find_biggest_available_numa_node(int *ncores, int **cores)
{
    hwloc_topology_t topology;

    int nNUMAnodes;
    int *navail_cores;
    int biggest_avail_numa = 0;
    int biggest_avail_ncores = 0;
    int biggest_avail_stride = 0;
    int i, j, k, w, workers_offset=0;

    for (w=STARPU_CUDA_WORKER ; w<STARPU_ANY_WORKER ; w++)
        workers_offset += starpu_worker_get_count_by_type(w);

    hwloc_topology_init(&topology);
    hwloc_topology_load(topology);

    nNUMAnodes = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_NODE);
    navail_cores=(int*)malloc(sizeof(int)*nNUMAnodes);

    for (i=0 ; i<nNUMAnodes ; i++) {
        //printf("Numa node %d = ", i);
        navail_cores[i]=0;
        hwloc_obj_t NUMAnode = hwloc_get_obj_by_type(topology, HWLOC_OBJ_NODE, i);

        int nPUs = hwloc_get_nbobjs_inside_cpuset_by_type(topology, NUMAnode->cpuset, HWLOC_OBJ_PU);

        for (k=0 ; k<nPUs ; k++ ) {
            hwloc_obj_t child = hwloc_get_obj_inside_cpuset_by_type(topology, NUMAnode->cpuset, HWLOC_OBJ_PU, k);

            int found = 0;
            for (w=STARPU_CUDA_WORKER ; w<STARPU_ANY_WORKER && !found; w++) {
                int nworkers = starpu_worker_get_count_by_type(w);
                int *workers = (int*) malloc(sizeof(int) * nworkers);

                starpu_worker_get_ids_by_type(w, workers, nworkers);

                for(j=0 ; j<nworkers && !found ; j++) {
                    if (child->logical_index == starpu_worker_get_bindid(workers[j])){
                        found = 1;
                    }
                }

                free(workers);
            }
            if (!found)
                navail_cores[i]++;
        }

        //I prefere taking the last numa node if the same number of PUs are available
        if (navail_cores[i] >= biggest_avail_ncores) {
            biggest_avail_numa = i;
            biggest_avail_ncores = navail_cores[i];
        }
    }

    hwloc_topology_destroy(topology);

    //Compute stride for later
    for (i=0 ; i<nNUMAnodes && i < biggest_avail_numa; i++)
        biggest_avail_stride += navail_cores[i];

    *ncores = biggest_avail_ncores;
    (*cores) = (int*)malloc(biggest_avail_ncores*sizeof(int));

    for (i=0 ; i<biggest_avail_ncores ; i++){
        (*cores)[i] = biggest_avail_stride + workers_offset + i;
        //printf("Workers_offset... %d ;; Parallel panel : worker[%d]=%d\n", workers_offset, i, (*cores)[i]);
    }

    free(navail_cores);
}
#else
void find_biggest_available_numa_node(int *ncores, int **cores)
{
    return;
}
#endif


void RUNTIME_unalloc_threads(MORSE_option_t *options)
{
#if (STARPU_MAJOR_VERSION == 1 && STARPU_MINOR_VERSION >= 2) || STARPU_MAJOR_VERSION > 1
    starpu_sched_ctx_delete(options->panel_threads);
    options->panel_threads = -1;
#endif
}

void RUNTIME_alloc_threads(MORSE_option_t *options, int ncores, int *cores)
{
#if (STARPU_MAJOR_VERSION == 1 && STARPU_MINOR_VERSION >= 2) || STARPU_MAJOR_VERSION > 1
    if (options->panel_threads < 0)
    {
        if (cores == NULL)
            find_biggest_available_numa_node(&ncores, &cores);
        options->panel_threads = starpu_sched_ctx_create(cores, ncores, "ctx_panel", STARPU_SCHED_CTX_AWAKE_WORKERS,  0);
    }
#endif
}

