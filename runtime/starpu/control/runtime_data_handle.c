/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2016 Inria. All rights reserved.
 * @copyright (c) 2012-2014, 2016 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 * @file runtime_data_handle.c
 *
 *  MORSE auxiliary routines
 *  MORSE is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver
 *
 * @author Terry Cojean
 * @date 2017-02-11
 *
 **/
#include "runtime/starpu/include/morse_starpu.h"

void RUNTIME_data_alloc(MORSE_data_handle_t* data_handle)
{
    data_handle->data_handle = (starpu_data_handle_t*)malloc(sizeof(starpu_data_handle_t));
    return;
}

void RUNTIME_data_free(MORSE_data_handle_t* data_handle)
{
    free(data_handle->data_handle);
    return;
}

void RUNTIME_vector_data_register(MORSE_data_handle_t* data_handle, uint32_t home_node,
                                  uintptr_t ptr, uint32_t nx, size_t elemsize)
{
    starpu_vector_data_register((starpu_data_handle_t*)data_handle->data_handle, home_node, ptr, nx, elemsize);

    data_handle->ptr = (void*)ptr;
    return;
}

void RUNTIME_matrix_data_register(MORSE_data_handle_t* data_handle, uint32_t home_node,
                                  uintptr_t ptr, uint32_t ld, uint32_t nx,
                                  uint32_t ny, size_t elemsize)
{
    starpu_matrix_data_register((starpu_data_handle_t*)data_handle->data_handle, home_node, ptr, ld, nx, ny, elemsize);
    data_handle->ptr = (void*)ptr;
    return;
}

void RUNTIME_data_acquire(MORSE_data_handle_t *data_handle)
{
    starpu_data_acquire(*(starpu_data_handle_t*)data_handle->data_handle, STARPU_R);
}

void RUNTIME_data_release(MORSE_data_handle_t *data_handle)
{
    starpu_data_release(*(starpu_data_handle_t*)data_handle->data_handle);
}

void RUNTIME_data_unregister(MORSE_data_handle_t* data_handle)
{
    starpu_data_handle_t *handle = (starpu_data_handle_t*)data_handle->data_handle;
    starpu_data_unregister(*handle);
    return;
}

static
void callback(void *arg)
{
    starpu_data_release(*((starpu_data_handle_t*)arg));
}


void RUNTIME_data_acquire_all(MORSE_desc_t *desc, int mode)
{
    starpu_data_handle_t* handle = desc->schedopt;
    int lmt = desc->lmt;
    int lnt = desc->lnt;
    int m, n;

    for (n = 0; n < lnt; n++)
        for (m = 0; m < lmt; m++)
        {
            if ( (*handle == NULL) ||
                 !morse_desc_islocal( desc, m, n ) )
            {
                handle++;
                continue;
            }
            starpu_data_acquire_cb(*handle, mode, callback, handle);
            handle++;
        }
}
